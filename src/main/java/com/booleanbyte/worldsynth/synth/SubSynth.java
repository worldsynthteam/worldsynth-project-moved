package com.booleanbyte.worldsynth.synth;

import com.booleanbyte.worldsynth.extent.WorldExtentManager;
import com.booleanbyte.worldsynth.synth.io.Element;

public class SubSynth extends Synth {
	
	private Synth parrentSynth;
	
	public SubSynth(String name, Synth parrentSynth) {
		super(name);
		this.parrentSynth = parrentSynth;
	}
	
	public SubSynth(Element element, Synth parrentSynth) {
		super("");
		this.parrentSynth = parrentSynth;
		fromElement(element, parrentSynth);
	}
	
	public Synth getParrentSynth() {
		return parrentSynth;
	}
	
	public WorldExtentManager getExtentManager() {
		return parrentSynth.getExtentManager();
	}
}
