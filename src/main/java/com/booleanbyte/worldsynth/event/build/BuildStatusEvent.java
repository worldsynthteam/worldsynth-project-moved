package com.booleanbyte.worldsynth.event.build;

import java.util.EventObject;

import com.booleanbyte.worldsynth.event.build.BuildStatus.BuildState;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapper;

public class BuildStatusEvent extends EventObject {
	private static final long serialVersionUID = 5609339495579651489L;
	
	private final BuildStatus status;
	
	public BuildStatusEvent(BuildStatus status, Object source) {
		this(status.getBuildState(), status.getMessage(), status.getDevice(), status.getRequest(), status.getBuildThread(), source);
	}
	
	public BuildStatusEvent(BuildState buildState, String message, ModuleWrapper device, ModuleOutputRequest request, Thread buildThread, Object source) {
		super(source);
		status = new BuildStatus(buildState, message, device, request, buildThread);
	}
	
	public BuildStatus getStatus() {
		return status;
	}
}
