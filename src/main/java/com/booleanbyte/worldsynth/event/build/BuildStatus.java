package com.booleanbyte.worldsynth.event.build;

import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapper;

public class BuildStatus {
	private final String message;
	private final BuildState buildState;
	private final ModuleWrapper device;
	private final ModuleOutputRequest request;
	private final Thread buildThread;
	
	public BuildStatus(BuildState buildState, String message, ModuleWrapper device, ModuleOutputRequest request, Thread buildThread) {
		this.message = message;
		this.buildState = buildState;
		this.device = device;
		this.request = request;
		this.buildThread = buildThread;
	}
	
	public String getMessage() {
		return message;
	}
	
	public BuildState getBuildState() {
		return buildState;
	}
	
	public ModuleWrapper getDevice() {
		return device;
	}
	
	public ModuleOutputRequest getRequest() {
		return request;
	}
	
	public Thread getBuildThread() {
		return buildThread;
	}
	
	@Override
	public String toString() {
		return getBuildState().getDefaultMessage().replaceAll("%device", getDevice().toString());
	}
	
	public enum BuildState {
		REGISTERED("Registered request for building: %device"),
		PREPARING_INPUTREQUESTS("Preparing input requests for: %device"),
		TRANSLATING_INPUTREQUESTS_TO_OUTPUTREQUESTS("Tranlating request from: &device"),
		PREPARING_INPUT_DATA("Preparing input data to: %device"),
		BUILDING("Building: %device"),
		DONE_BUILDING("Done building: %device");
		
		private final String defaultMessage;
		
		private BuildState(String defaultMessage) {
			this.defaultMessage = defaultMessage;
		}
		
		public String getDefaultMessage() {
			return defaultMessage;
		}
	}
}
