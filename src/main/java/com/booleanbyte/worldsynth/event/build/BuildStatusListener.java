package com.booleanbyte.worldsynth.event.build;

import java.util.EventListener;

public interface BuildStatusListener extends EventListener {
	
	public void buildUpdate(BuildStatusEvent event);
	
}
