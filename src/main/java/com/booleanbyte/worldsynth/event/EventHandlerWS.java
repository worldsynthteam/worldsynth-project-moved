package com.booleanbyte.worldsynth.event;

import java.util.EventListener;
import java.util.EventObject;

public interface EventHandlerWS<T extends EventObject> extends EventListener {
	void handle(T event);
}
