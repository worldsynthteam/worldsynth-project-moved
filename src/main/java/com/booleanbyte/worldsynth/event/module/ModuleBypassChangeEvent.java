package com.booleanbyte.worldsynth.event.module;

import javafx.event.Event;

public class ModuleBypassChangeEvent extends Event {
	private static final long serialVersionUID = -7955269316224933178L;
	
	private final boolean oldBypassValue;
	private final boolean newBypassValue;
	
	public ModuleBypassChangeEvent(boolean oldBypassValue, boolean newBypassValue) {
		super(null);
		this.oldBypassValue = oldBypassValue;
		this.newBypassValue = newBypassValue;
	}
	
	public boolean getOldBypassValue() {
		return oldBypassValue;
	}
	
	public boolean getNewBypassValue() {
		return newBypassValue;
	}
}
