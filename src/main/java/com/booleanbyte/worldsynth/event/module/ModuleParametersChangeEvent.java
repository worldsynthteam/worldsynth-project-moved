package com.booleanbyte.worldsynth.event.module;

import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.Event;

public class ModuleParametersChangeEvent extends Event {
	private static final long serialVersionUID = -7955269316224933178L;
	
	private final Element oldParametersElement;
	private final Element newParametersElement;
	
	public ModuleParametersChangeEvent(Element oldParametersElement, Element newParametersElement) {
		super(null);
		this.oldParametersElement = oldParametersElement;
		this.newParametersElement = newParametersElement;
	}
	
	public Element getOldParametersElement() {
		return oldParametersElement;
	}
	
	public Element getNewParametersElement() {
		return newParametersElement;
	} 
	
}
