package com.booleanbyte.worldsynth.event.module;

import javafx.event.Event;

public class ModuleIoChangeEvent extends Event {
	private static final long serialVersionUID = 3477520059797146016L;

	public ModuleIoChangeEvent() {
		super(null);
	}
}
