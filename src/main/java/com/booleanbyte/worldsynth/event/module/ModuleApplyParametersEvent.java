package com.booleanbyte.worldsynth.event.module;

import javafx.event.Event;

public class ModuleApplyParametersEvent extends Event {
	private static final long serialVersionUID = -6577128138770823364L;

	public ModuleApplyParametersEvent() {
		super(null);
	}
}
