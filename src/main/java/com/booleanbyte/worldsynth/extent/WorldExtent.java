package com.booleanbyte.worldsynth.extent;

import java.util.ArrayList;

import com.booleanbyte.worldsynth.synth.io.Element;
import com.sun.javafx.event.EventHandlerManager;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.event.EventType;

public class WorldExtent {

	private static long nextExtentId = 1;
	private long extentId;

	private boolean blockPreviewUpdateEvent = false;
	private final EventHandlerManager eventHandlerManager = new EventHandlerManager(this);

	private final SimpleStringProperty name = new SimpleStringProperty("Unnamed extent");
	private final SimpleBooleanProperty sizeRatioLock = new SimpleBooleanProperty(true);
	private final SimpleDoubleProperty sizeRatio = new SimpleDoubleProperty(1.0);

	private final SimpleDoubleProperty x = new SimpleDoubleProperty(0.0);
	private final SimpleDoubleProperty y = new SimpleDoubleProperty(0.0);
	private final SimpleDoubleProperty z = new SimpleDoubleProperty(0.0);

	private final SimpleDoubleProperty width = new SimpleDoubleProperty(256.0);
	private final SimpleDoubleProperty height = new SimpleDoubleProperty(256.0);
	private final SimpleDoubleProperty length = new SimpleDoubleProperty(256.0);

	public WorldExtent(String name) {
		this(name, 0.0, 0.0, 256.0);
	}

	public WorldExtent(String name, double x, double z, double size) {
		this(name, x, 0, z, size, 256.0, size, true);
	}

	public WorldExtent(String name, double x, double z, double width, double length) {
		this(name, x, 0, z, width, 256.0, length, true);
	}

	public WorldExtent(String name, double x, double y, double z, double width, double height, double length, boolean sizeRatioLock) {
		this.name.set(name);
		this.x.set(x);
		this.y.set(y);
		this.z.set(z);
		this.sizeRatioLock.set(sizeRatioLock);
		this.sizeRatio.set(width / length);
		this.width.set(width);
		this.height.set(height);
		this.length.set(length);

		extentId = nextExtentId++;
		
		//Add listeners to retain correct ratio aspects
		lengthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(getSizeRatioLock()) {
					widthProperty().set(newValue.doubleValue() * sizeRatio.get());
				}
				else {
					sizeRatio.set(getWidth() / newValue.doubleValue());
				}
			}
		});
		
		widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(getSizeRatioLock()) {
					lengthProperty().set(newValue.doubleValue() / sizeRatio.get());
				}
				else {
					sizeRatio.set(newValue.doubleValue() / getLength());
				}
			}
		});

		extentId = nextExtentId++;
	}

	public WorldExtent(Element element) {
		fromElement(element);
		
		//Add listeners to retain correct ratio aspects
		lengthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(getSizeRatioLock()) {
					widthProperty().set(newValue.doubleValue() * sizeRatio.get());
				}
				else {
					sizeRatio.set(getWidth() / newValue.doubleValue());
				}
			}
		});
		
		widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(getSizeRatioLock()) {
					lengthProperty().set(newValue.doubleValue() / sizeRatio.get());
				}
				else {
					sizeRatio.set(newValue.doubleValue() / getLength());
				}
			}
		});
	}

	public long getId() {
		return extentId;
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getName() {
		return name.get();
	}

	public BooleanProperty sizeRatioLockProperty() {
		return sizeRatioLock;
	}

	public void setSizeRatioLock(boolean symetryLock) {
		this.sizeRatioLock.set(symetryLock);
	}

	public boolean getSizeRatioLock() {
		return sizeRatioLock.get();
	}

	public double getSizeRatio() {
		return sizeRatio.get();
	}

	public DoubleProperty xProperty() {
		return x;
	}

	public void setX(double x) {
		this.x.set(x);
	}

	public double getX() {
		return x.get();
	}

	public DoubleProperty yProperty() {
		return y;
	}

	public void setY(double y) {
		this.y.set(y);
	}

	public double getY() {
		return y.get();
	}

	public DoubleProperty zProperty() {
		return z;
	}

	public void setZ(double z) {
		this.z.set(z);
	}

	public double getZ() {
		return z.get();
	}

	public DoubleProperty widthProperty() {
		return width;
	}

	public void setWidth(double width) {
		this.width.set(width);
	}

	public double getWidth() {
		return width.get();
	}

	public DoubleProperty heightProperty() {
		return height;
	}

	public void setHeight(double height) {
		this.height.set(height);
	}

	public double getHeight() {
		return height.get();
	}

	public DoubleProperty lengthProperty() {
		return length;
	}

	public void setLength(double length) {
		this.length.set(length);
	}

	public double getLength() {
		return length.get();
	}

	public double getArea() {
		return getLength() * getLength();
	}

	public double getVolume() {
		return getArea() * getHeight();
	}

	public boolean blockPreviewUpdateEvent() {
		return blockPreviewUpdateEvent;
	}

	public final <T extends ExtentEvent> void addEventHandler(final EventType<T> eventType, final EventHandler<? super T> eventHandler) {
		getEventHandlerManager().addEventHandler(eventType, eventHandler);
	}

	public final <T extends ExtentEvent> void removeEventHandler(final EventType<T> eventType, final EventHandler<? super T> eventHandler) {
		getEventHandlerManager().removeEventHandler(eventType, eventHandler);
	}

	private EventHandlerManager getEventHandlerManager() {
		return eventHandlerManager;
	}

	void fireExtentAddedEvent() {
		eventHandlerManager.dispatchBubblingEvent(new ExtentEvent(ExtentEvent.EXTENT_ADDED, this));
	}

	void fireExtentEditedEvent() {
		eventHandlerManager.dispatchBubblingEvent(new ExtentEvent(ExtentEvent.EXTENT_EDITED, this));
	}

	void fireExtentRemovedEvent() {
		eventHandlerManager.dispatchBubblingEvent(new ExtentEvent(ExtentEvent.EXTENT_REMOVED, this));
	}

	/**
	 * Check if the coordinates is contained inside this extent.
	 * 
	 * @param x
	 * @param z
	 * @return true if the coordinate is contained inside the extent, false
	 *         otherwise.
	 */
	public boolean containsCoordinate(double x, double z) {
		if(x < getX() || x >= getX() + getWidth()) {
			return false;
		}
		else if(z < getZ() || z >= getZ() + getLength()) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the coordinates iscontained inside this extent.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @return true if the coordinate is contained inside the extent, false
	 *         otherwise.
	 */
	public boolean containsCoordinate(double x, double y, double z) {
		if(x < getX() || x >= getX() + getWidth()) {
			return false;
		}
		else if(y < getY() || y >= getY() + getHeight()) {
			return false;
		}
		else if(z < getZ() || z >= getZ() + getLength()) {
			return false;
		}
		return true;
	}

	public double distanceToClosestConer(double x, double z) {
		// Dist NW
		double dx = x - getX();
		double dz = z - getZ();
		double dist = Math.sqrt(dx * dx + dz * dz);
		// Dist NE
		dx = x - (getX() + getWidth());
		dz = z - getZ();
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));
		// Dist SW
		dx = x - getX();
		dz = z - (getZ() + getLength());
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));
		// Dist SE
		dx = x - (getX() + getWidth());
		dz = z - (getZ() + getLength());
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));

		return dist;
	}

	public ExtentCorner closestCorner(double x, double z) {
		// NW
		double dx = x - getX();
		double dz = z - getZ();
		double d = Math.sqrt(dx * dx + dz * dz);
		double minDist = d;
		ExtentCorner closestCorner = ExtentCorner.NW;
		// NE
		dx = x - (getX() + getWidth());
		dz = z - getZ();
		d = Math.sqrt(dx * dx + dz * dz);
		if(d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.NE;
		}
		// SW
		dx = x - getX();
		dz = z - (getZ() + getLength());
		d = Math.sqrt(dx * dx + dz * dz);
		if(d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.SW;
		}
		// SE
		dx = x - (getX() + getWidth());
		dz = z - (getZ() + getLength());
		d = Math.sqrt(dx * dx + dz * dz);
		if(d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.SE;
		}

		return closestCorner;
	}

	public String toString() {
		return getName();
	}

	@Override
	public WorldExtent clone() {
		return new WorldExtent(getName(), getX(), getY(), getZ(), getWidth(), getHeight(), getLength(), getSizeRatioLock());
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof WorldExtent) {
			WorldExtent comparableExtent = (WorldExtent) obj;
			if(comparableExtent.getName().equals(getName()) && comparableExtent.getX() == getX() && comparableExtent.getY() == getY() && comparableExtent.getZ() == getZ() && comparableExtent.getWidth() == getWidth() && comparableExtent.getHeight() == getHeight()
					&& comparableExtent.getLength() == getLength()) {
				return true;
			}
		}
		return false;
	}

	public void cloneValuesFrom(WorldExtent extent) {
		//Build a list over differences so we can block the preview update for all but the last change
		ArrayList<DoubleProperty[]> diffList = new ArrayList<DoubleProperty[]>();
		if(x.get() != extent.getX())
			diffList.add(new DoubleProperty[] { xProperty(), extent.xProperty() });
		if(y.get() != extent.getY())
			diffList.add(new DoubleProperty[] { yProperty(), extent.yProperty() });
		if(z.get() != extent.getZ())
			diffList.add(new DoubleProperty[] { zProperty(), extent.zProperty() });
		if(width.get() != extent.getWidth())
			diffList.add(new DoubleProperty[] { widthProperty(), extent.widthProperty() });
		if(height.get() != extent.getHeight())
			diffList.add(new DoubleProperty[] { heightProperty(), extent.heightProperty() });
		if(length.get() != extent.getLength() && !sizeRatioLock.get())
			diffList.add(new DoubleProperty[] { lengthProperty(), extent.lengthProperty() });
		if(diffList.size() == 0)
			return;

		blockPreviewUpdateEvent = true;
		DoubleProperty[] diffProperties;
		for(int i = 0; i < diffList.size() - 1; i++) {
			diffProperties = diffList.get(i);
			diffProperties[0].set(diffProperties[1].get());
		}
		// Disable the blocking before the last change is updated so the previews will update
		blockPreviewUpdateEvent = false;
		diffProperties = diffList.get(diffList.size() - 1);
		diffProperties[0].set(diffProperties[1].get());
	}

	public Element toElement() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();

		paramenterElements.add(new Element("id", String.valueOf(extentId)));
		paramenterElements.add(new Element("name", getName()));
		paramenterElements.add(new Element("sizeratiolock", String.valueOf(getSizeRatioLock())));
		paramenterElements.add(new Element("x", String.valueOf(getX())));
		paramenterElements.add(new Element("y", String.valueOf(getY())));
		paramenterElements.add(new Element("z", String.valueOf(getZ())));
		paramenterElements.add(new Element("width", String.valueOf(getWidth())));
		paramenterElements.add(new Element("height", String.valueOf(getHeight())));
		paramenterElements.add(new Element("length", String.valueOf(getLength())));

		Element moduleElement = new Element("extent", paramenterElements);
		return moduleElement;
	}

	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("id")) {
				extentId = Long.parseLong(e.content);
				nextExtentId = Math.max(nextExtentId, extentId + 1);
			}
			else if(e.tag.equals("name")) {
				setName(e.content);
			}
			else if(e.tag.equals("sizeratiolock")) {
				setSizeRatioLock(Boolean.parseBoolean(e.content));
			}
			else if(e.tag.equals("x")) {
				setX(Double.parseDouble(e.content));
			}
			else if(e.tag.equals("y")) {
				setY(Double.parseDouble(e.content));
			}
			else if(e.tag.equals("z")) {
				setZ(Double.parseDouble(e.content));
			}
			else if(e.tag.equals("width")) {
				setWidth(Double.parseDouble(e.content));
			}
			else if(e.tag.equals("height")) {
				setHeight(Double.parseDouble(e.content));
			}
			else if(e.tag.equals("length")) {
				setLength(Double.parseDouble(e.content));
			}
		}
	}

	public enum ExtentCorner {
		NE, NW, SE, SW;
	}
}
