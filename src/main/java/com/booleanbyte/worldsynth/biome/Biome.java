package com.booleanbyte.worldsynth.biome;

import com.booleanbyte.worldsynth.common.color.WsColor;

import javafx.scene.paint.Color;

public class Biome implements Comparable<Biome> {
	private String name;
	private String idName;
	private int id;
	private WsColor color;
	
	public static Biome NULL = new Biome("", "", -1, null);
	
	public Biome(String name, String idName, int id, WsColor color) {
		this.name = name;
		this.idName = idName;
		this.id = id;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIdName() {
		return idName;
	}
	
	public int getId() {
		return id;
	}
	
	public WsColor getWsColor() {
		return color;
	}
	
	public Color getFxColor() {
		return color.getFxColor();
	}
	
	public int getInternalId() {
		return BiomeRegistry.getInternalId(this);
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Biome comp) {
		return getName().compareTo(comp.getName());
	}
}
