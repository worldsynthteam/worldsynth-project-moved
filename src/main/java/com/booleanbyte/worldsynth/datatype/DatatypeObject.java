package com.booleanbyte.worldsynth.datatype;

import com.booleanbyte.worldsynth.customobject.CustomObject;
import com.booleanbyte.worldsynth.customobject.LocatedCustomObject;

import javafx.scene.paint.Color;

public class DatatypeObject extends AbstractDatatype {
	
	public LocatedCustomObject object;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeObject() {
	}
	
	/**
	 * @param x The x coordinate of the custom object carried by this datatype
	 * @param y The y coordinate of the custom object carried by this datatype
	 * @param z The z coordinate of the custom object carried by this datatype
	 * @param seed The seed of the custom object carried by this datatype
	 */
	public DatatypeObject(double x, double y, double z, long seed) {
		object = new LocatedCustomObject(x, y, z, seed);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(0, 43, 170);
	}

	@Override
	public String getDatatypeName() {
		return "Object";
	}
	
	@Override
	public AbstractDatatype clone() {
		DatatypeObject dvb = new DatatypeObject();
		dvb.object = object.clone();
		return dvb;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeObject(0.0, 0.0, 0.0, 0);
	}
	
	public double getCoordinateX() {
		return object.getX();
	}
	
	public double getCoordinateY() {
		return object.getY();
	}
	
	public double getCOordianteZ() {
		return object.getZ();
	}
	
	public long getSeed() {
		return object.getSeed();
	}
	
	public CustomObject getObject() {
		return object.getObject();
	}
	
	public void setObject(CustomObject object) {
		this.object.setObject(object);
	}
}
