package com.booleanbyte.worldsynth.datatype;

import com.booleanbyte.worldsynth.standalone.ui.preview.AbstractPreviewRender;

import javafx.scene.paint.Color;

public abstract class AbstractDatatype {
	
	public AbstractDatatype() {
		
	}
	
	/**
	 * @return The {@link Color} that represents the datatype at the IO rendering in the UI
	 */
	public abstract Color getDatatypeColor();
	
	/**
	 * @return The name of the datattype
	 */
	public abstract String getDatatypeName();
	
	public abstract AbstractDatatype clone();
	
	/**
	 * Used by datatypes not part of the standard library to set its preview render class.
	 * Datatypes part of the standard library does not register their renderer with this
	 * since all their render classes are directly registered in the WorldSynth editor to
	 * minimize the amount of dependencies in the WorldSynth core.
	 * <br>
	 * Registering a renderer using this method requires the method to be overridden to return
	 * a new instance of the appropriate renderer.
	 * 
	 * @return An instance of the appropriate renderer for the datatype.
	 */
	public AbstractPreviewRender getPreviewRender() {
		return null;
	}
	
	/**
	 * This method is to be implemented by the implementor of a datatype to define the way a
	 * the datatype in question is mapped to a preview extent in the patcher when requesting
	 * a preview of the datatype. The method has parameters corresponding to the preview extent
	 * parameters that is intended to be used to construct a request instance of the datatype
	 * to be used with the outputrequest for a module with this datatype as the preview output.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param length
	 * @param resolution
	 * @return a request instance of the datatype this is implemented for
	 */
	public abstract AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution);
}
