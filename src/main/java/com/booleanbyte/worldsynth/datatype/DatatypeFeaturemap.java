package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeFeaturemap extends AbstractDatatype {
	
	/**
	 * Array of {x, z}
	 */
	public double[][] points;
	
	/**
	 * Array of feature seed for the feature points
	 */
	public long[] pointSeeds;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the feature bounding box the featurepoints are inside of
	 */
	public double width, length;
	
	/**
	 * Resolution is not mainly used while generating or performing modifications to featuremaps, but is used when
	 * requesting other datatypes as inputs for the various operations.
	 */
	public double resolution;
	
	public DatatypeFeaturemap() {
	}
	
	public DatatypeFeaturemap(double x, double z, double width, double length) {
		this(x, z, width, length, 1.0);
	}
	
	public DatatypeFeaturemap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 85, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Featuremap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeaturemap dvb = new DatatypeFeaturemap(x, z, width, length, resolution);
		if(points != null) {
			dvb.points = points.clone();
		}
		if(pointSeeds != null) {
			dvb.pointSeeds = pointSeeds.clone();
		}
		return dvb;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeaturemap(x, z, width, length, resolution);
	}
}
