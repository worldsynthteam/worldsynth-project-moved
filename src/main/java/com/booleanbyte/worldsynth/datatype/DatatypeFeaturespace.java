package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeFeaturespace extends AbstractDatatype {
	
	/**
	 * Array of {x, y, z}
	 */
	public double[][] points;
	
	/**
	 * Array of feature seed for the feature points
	 */
	public long[] pointSeeds;
	
	/**
	 * Corner coordinate
	 */
	public double x, y, z;
	
	/**
	 * Unit size of the feature bounding box the featurepoints are inside of
	 */
	public double width, height, length;
	
	/**
	 * Resolution is not mainly used while generating or performing modifications to featuremaps, but is used when
	 * requesting other datatypes as inputs for the various operations.
	 */
	public double resolution;
	
	public DatatypeFeaturespace() {
	}
	
	public DatatypeFeaturespace(double x, double y, double z, double width, double height, double length) {
		this(x, y, z, width, height, length, 1.0);
	}
	
	public DatatypeFeaturespace(double x, double y, double z, double width, double height, double length, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.length = length;
		this.resolution = resolution;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(143, 65, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Featurespace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeaturespace dvb = new DatatypeFeaturespace(x, y, z, width, height, length, resolution);
		if(points != null) {
			dvb.points = points.clone();
		}
		if(pointSeeds != null) {
			dvb.pointSeeds = pointSeeds.clone();
		}
		return dvb;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeaturespace(x, y, z, width, height, length, resolution);
	}
}
