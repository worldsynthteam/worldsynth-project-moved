package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeHeightmap extends AbstractDatatype {
	
	public float[][] heightMap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the heightmap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per heightmap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the heightmap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeHeightmap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the heightmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeHeightmap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeHeightmap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Heightmap";
	}

	@Override
	public DatatypeHeightmap clone() {
		DatatypeHeightmap dhm = new DatatypeHeightmap(x, z, width, length, resolution);
		if(heightMap != null) {
			float[][] hm = new float[mapPointsWidth][mapPointsLength];
			for(int u = 0; u < mapPointsWidth; u++) {
				for(int v = 0; v < mapPointsWidth; v++) {
					hm[u][v] = heightMap[u][v];
				}
			}
			dhm.heightMap = hm;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeHeightmap(x, z, width, length, resolution);
	}
}
