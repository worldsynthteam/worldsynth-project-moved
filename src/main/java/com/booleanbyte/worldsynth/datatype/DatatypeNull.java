package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeNull extends AbstractDatatype {
	
	public double data;

	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 255, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Unknown";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeNull df = new DatatypeNull();
		df.data = data;
		return df;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return null;
	}
}
