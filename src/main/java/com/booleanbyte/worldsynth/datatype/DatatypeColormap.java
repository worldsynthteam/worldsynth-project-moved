package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeColormap extends AbstractDatatype {
	
	public float[][][] colorMap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the colormap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per colormap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the colormap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeColormap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the colormap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeColormap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeColormap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 255, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Colormap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeColormap dhm = new DatatypeColormap(x, z, width, length, resolution);
		if(colorMap != null) {
			float[][][] hm = colorMap.clone();
			dhm.colorMap = hm;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeColormap(x, z, width, length, resolution);
	}
}
