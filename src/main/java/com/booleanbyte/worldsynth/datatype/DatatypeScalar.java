package com.booleanbyte.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeScalar extends AbstractDatatype {
	
	public double data;

	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 85, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Scalar";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeScalar df = new DatatypeScalar();
		df.data = data;
		return df;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeScalar();
	}
}
