package com.booleanbyte.worldsynth.datatype;

import com.booleanbyte.worldsynth.customobject.LocatedCustomObject;

import javafx.scene.paint.Color;

public class DatatypeObjects extends AbstractDatatype {
	
	public LocatedCustomObject[] objects;
	
	/**
	 * Corner coordinate
	 */
	public double x, y, z;
	
	/**
	 * Unit size of the valuespace
	 */
	public double width, height, lenght;
	
	/**
	 * The resolutions of units per valuespace point
	 */
	public double resolution;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeObjects() {
	}
	
	public DatatypeObjects(double[] objectx, double[] objecty, double[] objectz, long[] objectseed, double x, double y, double z, double width, double height, double lenght, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.lenght = lenght;
		this.resolution = resolution;
		
		if(objectx.length == objecty.length && objecty.length == objectz.length && objectz.length == objectseed.length) {
			objects = new LocatedCustomObject[objectx.length];
			for(int i = 0; i < objectx.length; i++) {
				objects[i] = new LocatedCustomObject(objectx[i], objecty[i], objectz[i], objectseed[i]);
			}
		}
	}
	
	public DatatypeObjects(LocatedCustomObject[] objects, double x, double y, double z, double width, double height, double lenght, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.lenght = lenght;
		this.resolution = resolution;
		
		this.objects = objects;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(128, 0, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Objects";
	}

	@Override
	public AbstractDatatype clone() {
		if(this.objects != null) {
			LocatedCustomObject[] objects = new LocatedCustomObject[this.objects.length];
			for(int i = 0; i < objects.length; i++) {
				objects[i] = this.objects[i].clone();
			}
			return new DatatypeObjects(objects, x, y, z, width, height, lenght, resolution);
		}
		return new DatatypeObjects();
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		LocatedCustomObject[] objects = new LocatedCustomObject[10];
		for(int i = 0; i < objects.length; i++) {
			objects[i] = new LocatedCustomObject(0, 0, 0, i);
		}
		return new DatatypeObjects(objects, x, y, z, width, height, length, resolution);
	}
	
	public LocatedCustomObject[] getLocatedObjects() {
		return objects;
	}
}
