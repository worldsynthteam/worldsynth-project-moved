package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapCombiner extends AbstractModule {
	
	Operation operation = Operation.ADDITION;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Check if both inputs are available
		if(inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary heightmap
		float[][] primaryMap = ((DatatypeHeightmap) inputs.get("primary")).heightMap;
		float[][] secondaryMap = ((DatatypeHeightmap) inputs.get("secondary")).heightMap;
		
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
				
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float i0 = primaryMap[u][v];
				float i1 = secondaryMap[u][v];
				float o = 0.0f;
				
				switch (operation) {
				case ADDITION:
					o = i0 + i1;
					break;
				case SUBTRACTION:
					o = i0 - i1;
					break;
				case MULTIPLICATION:
					o = i0 * i1;
					break;
				case DIVISION:
					o = i0 / i1;
					break;
				case MODULO:
					o = i0 % i1;
					break;
				case AVERAGE:
					o = (i0 + i1) / 2;
					break;
				case MAX:
					o = Math.max(i0, i1);
					break;
				case MIN:
					o = Math.min(i0, i1);
					break;
				case OWERFLOW:
					o = (i0 + i1);
					o = o % 1.0f;
					break;
				case UNDERFLOW:
					o = (i0 - i1);
					o = o < 0.0f ? o + 1.0f : o;
				}
				
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				map[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v] + primaryMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.name().substring(0, 3);
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Secondary input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		EnumParameterDropdownSelector<Operation> parameterOperation = new EnumParameterDropdownSelector<Operation>("Arithmetic operation", Operation.class, operation);
		
		try {
			parameterOperation.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			operation = parameterOperation.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("operation", operation.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("operation")) {
				for(Operation type: Operation.values()) {
					if(e.content.equals(type.name())) {
						operation = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Operation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN, OWERFLOW, UNDERFLOW;
	}
}
