package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeMultitype;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.LongParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapWorley extends AbstractModule {
	
	private long seed;
	private double scale = 100;
	private double amplitude = 1;
	private double offset = 0;
	private DistanceFunction distanceFunction = DistanceFunction.EUCLIDEAN;
	private Feature feature = Feature.F1;
	private FeatureDistrubution featureDistrubution = FeatureDistrubution.VORONOI;
	
	private final double S60 = Math.sin(Math.toRadians(60));
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[][] dp;
	
	public ModuleHeightmapWorley() {
		
		seed = new Random().nextLong();
		
		dp = createPermutatationTable(permutationSize, 2, seed);
	}
	
	private int[][] createPermutatationTable(int size, int dim, long seed) {
		int[][] dp = new int[size*2][dim];
		
		for(int i = 0; i < dim; i++) {
			//Create a random generator with supplied seed
			Random r = new Random(seed + i);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for(int j = 0; j < size; j++) {
				valueTabel.add(j);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while(valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			for(int k = 0; k < dp.length; k++) {
				dp[k][i] = permutationTable[k%permutationSize];
			}
		}
		
		return dp;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in scale
		double scale = this.scale;
		if(inputs.get("scale") != null) {
			scale = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in amplitude
		double amplitudeValue = this.amplitude;
		float[][] amplitudeMap = null;
		if(inputs.get("amplitude") != null) {
			if(inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitudeValue = ((DatatypeScalar) inputs.get("amplitude")).data;
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).heightMap;
			}
		}
		
		//Read in offset
		double offsetValue = this.offset;
		float[][] offsetMap = null;
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = ((DatatypeScalar) inputs.get("offset")).data / 256.0;
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).heightMap;
			}
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has both amplitude and offset map
		if(amplitudeMap != null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeMap[u][v] * amplitudeValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has amplitude map
		else if(amplitudeMap != null && offsetMap == null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeMap[u][v] * amplitudeValue, offsetValue);
				}
			}
		}
		//Has offset map
		else if(amplitudeMap == null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeValue, offsetValue);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v];
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset) {
		return worley(x/scale, y/scale, amplitude, offset);
	}
	
	private double worley(double x, double y, double amplitude, double offset) {
		if(featureDistrubution == FeatureDistrubution.HEXAGON) {
			y *= 1.0/S60;
		}
		
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		//Calculate the local coordinates of the 9 closest featurepoints
		double[][] fx = new double[3][3];
		double[][] fy = new double[3][3];
		
		double[] dist = new double[9];
		
		for(int ix = 0; ix < 3; ix++) {
			for(int iy = 0; iy < 3; iy++) {
				int cx = inc(xi, ix-1);
				int cy = inc(yi, iy-1);
				
				int xh = hash(cx, cy, 0);
				int yh = hash(cx, cy, 1);
				
				double xoffset = 0.5;
				double yoffset = 0.5;
				
				if(featureDistrubution == FeatureDistrubution.VORONOI) {
					xoffset = (double) xh/(double) repeat;
					yoffset = (double) yh/(double) repeat;
				}
				else if(featureDistrubution == FeatureDistrubution.HEXAGON) {
					xoffset = 0;
					if(yi%2 == 0) {
						double c = 0.5;
						double m = 0 + Math.abs(iy-1);
						xoffset =  c * m;
					}
					else {
						double c = 0.5;
						double m = 1 - Math.abs(iy-1);
						xoffset =  c * m;
					}
					yoffset = 0;
				}
				
				fx[ix][iy] = (double) (ix-1) + xoffset;
				fy[ix][iy] = (double) (iy-1) + yoffset;
				
				double xdist = fx[ix][iy] - xf;
				double ydist = fy[ix][iy] - yf;
				if(featureDistrubution == FeatureDistrubution.HEXAGON) {
					ydist *= S60;
				}
				
				//Distance from point
				switch (distanceFunction) {
				case EUCLIDEAN:
					dist[ix*3 + iy] = Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
					break;
				case MANHATTAN:
					dist[ix*3 + iy] = Math.abs(xdist) + Math.abs(ydist);
					break;
				case EUCLIDEAN_SQUARED:
					dist[ix*3 + iy] = Math.pow(xdist, 2) + Math.pow(ydist, 2);
					break;
				case CHEBYSHEV:
					dist[ix*3 + iy] = Math.max(Math.abs(xdist), Math.abs(ydist));
					break;
				case MIN:
					dist[ix*3 + iy] = Math.min(Math.abs(xdist), Math.abs(ydist));
					break;
				}
			}
		}
		
		//Sort
		for(int i = 0; i < feature.getMaxFeature(); i++) {
			for(int j = 8; j > i; j--) {
				if(dist[j] < dist[j-1]) {
					double temp = dist[j];
					dist[j] = dist[j-1];
					dist[j-1] = temp;
				}
			}
		}
		
		double height = 0;
		switch (feature) {
		case F1:
			height = dist[0];
			break;
		case F2:
			height = dist[1];
			break;
		case F3:
			height = dist[2];
			break;
		case F2_F1:
			height = dist[1] - dist[0];
			break;
		case F3_F1:
			height = dist[2] - dist[0];
			break;
		case F3_F2:
			height = dist[2] - dist[1];
			break;
		}
		
		height *= amplitude;
		height += offset;
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	private int hash(int x, int y, int table) {
		return dp[dp[x][table]+y][table];
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "Worley";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		EnumParameterDropdownSelector<DistanceFunction> parameterDistanceFunction = new EnumParameterDropdownSelector<DistanceFunction>("Distance function", DistanceFunction.class, distanceFunction);
		EnumParameterDropdownSelector<Feature> parameterFeatureSelection = new EnumParameterDropdownSelector<Feature>("Feature selection", Feature.class, feature);
		EnumParameterDropdownSelector<FeatureDistrubution> parameterDistrubution = new EnumParameterDropdownSelector<FeatureDistrubution>("Distrubution", FeatureDistrubution.class, featureDistrubution);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterAmplitude.addToGrid(pane, 1);
			parameterOffset.addToGrid(pane, 2);
			parameterSeed.addToGrid(pane, 3);
			parameterDistanceFunction.addToGrid(pane, 4);
			parameterFeatureSelection.addToGrid(pane, 5);
			parameterDistrubution.addToGrid(pane, 6);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			distanceFunction = parameterDistanceFunction.getValue();
			feature = parameterFeatureSelection.getValue();
			featureDistrubution = parameterDistrubution.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			dp = createPermutatationTable(permutationSize, 2, seed);
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("distancefunction", String.valueOf(distanceFunction.name())));
		paramenterElements.add(new Element("feature", String.valueOf(feature.name())));
		paramenterElements.add(new Element("distrubution", String.valueOf(featureDistrubution.name())));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				dp = createPermutatationTable(permutationSize, 2, seed);
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("distancefunction")) {
				for(DistanceFunction type: DistanceFunction.values()) {
					if(e.content.equals(type.name())) {
						distanceFunction = type;
						break;
					}
				}
			}
			else if(e.tag.equals("feature")) {
				for(Feature type: Feature.values()) {
					if(e.content.equals(type.name())) {
						feature = type;
						break;
					}
				}
			}
			else if(e.tag.equals("distrubution")) {
				for(FeatureDistrubution type: FeatureDistrubution.values()) {
					if(e.content.equals(type.name())) {
						featureDistrubution = type;
						break;
					}
				}
			}
		}
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		EUCLIDEAN_SQUARED,
		CHEBYSHEV,
		MIN;
	}
	
	private enum Feature {
		F1(1),
		F2(2),
		F3(3),
		F2_F1(2),
		F3_F1(3),
		F3_F2(3);
		
		private final int maxFeature;
		
		private Feature(int maxFeature) {
			this.maxFeature = maxFeature;
		}
		
		int getMaxFeature() {
			return maxFeature;
		}
	}
	
	private enum FeatureDistrubution {
		VORONOI,
		HEXAGON,
		SQUARE;
	}
}
