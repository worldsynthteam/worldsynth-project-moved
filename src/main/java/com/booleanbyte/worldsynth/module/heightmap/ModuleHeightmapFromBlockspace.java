package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapFromBlockspace extends AbstractModule {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		float[][] map = new float[mpw][mpl];
		
		if(inputs.get("input") == null) {
			return null;
		}
		
		DatatypeBlockspace inputData = (DatatypeBlockspace) inputs.get("input");
		
		int[][][] inputBlockspaceMaterialId = inputData.blockspaceMaterialId;
		int blockspaceHeightLengt = inputBlockspaceMaterialId[0].length;
		
		for(int u = 0; u < mpw; u++) {
			for(int w = 0; w < mpl; w++) {
				for(int v = blockspaceHeightLengt-1; v >= 0; v--) {
					if(inputBlockspaceMaterialId[u][v][w] != 0) {
						double heightUnitsPerBlock = 1.0/(double)blockspaceHeightLengt;
						map[u][w] = (float)(v*heightUnitsPerBlock+heightUnitsPerBlock);
						break;
					}
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap hmrd = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeBlockspace blockspaceRequestData = new DatatypeBlockspace(hmrd.x, 0.0, hmrd.z, hmrd.width, 255.0, hmrd.length, hmrd.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), blockspaceRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap from blockspace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Primary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		
	}
}
