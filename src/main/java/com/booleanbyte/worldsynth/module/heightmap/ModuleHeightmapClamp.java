package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeMultitype;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapClamp extends AbstractModule {
	
	private float highClamp = 1;
	private float lowClamp = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).heightMap;
		
		//Read in high clamp
		float highClampValue = this.highClamp;
		float[][] highClampMap = null;
		if(inputs.get("high") != null) {
			if(inputs.get("high") instanceof DatatypeScalar) {
				highClampValue = (float) ((DatatypeScalar) inputs.get("high")).data / 256.0f;
			}
			else {
				highClampMap = ((DatatypeHeightmap) inputs.get("high")).heightMap;
			}
		}
		
		//Read in low clamp
		float lowClampValue = this.lowClamp;
		float[][] lowClampMap = null;
		if(inputs.get("low") != null) {
			if(inputs.get("low") instanceof DatatypeScalar) {
				lowClampValue = (float) ((DatatypeScalar) inputs.get("low")).data / 256.0f;
			}
			else {
				lowClampMap = ((DatatypeHeightmap)inputs.get("low")).heightMap;
			}
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] clampedMap = new float[mpw][mpl];
		
		//Has both high and low clamp map
		if(lowClampMap != null && highClampMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					clampedMap[u][v] = clamp(inputMap[u][v], lowClampMap[u][v], highClampMap[u][v]);
				}
			}
		}
		//Has low clamp map
		else if(lowClampMap != null && highClampMap == null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					clampedMap[u][v] = clamp(inputMap[u][v], lowClampMap[u][v], highClampValue);
				}
			}
		}
		//Has high clamp map
		else if(lowClampMap == null && highClampMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					clampedMap[u][v] = clamp(inputMap[u][v], lowClampValue, highClampMap[u][v]);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					clampedMap[u][v] = clamp(inputMap[u][v], lowClampValue, highClampValue);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					clampedMap[u][v] = clampedMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.heightMap = clampedMap;
		
		return requestData;
	}
	
	private float clamp(float height, float lowClamp, float highClamp) {
		if(height > highClamp) height = highClamp;
		else if(height < lowClamp) height = lowClamp;
		
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Clamp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "High clamp"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Low clamp"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		FloatParameterSlider parameterHighClamp = new FloatParameterSlider("High clamp", 0.0f, 1.0f, highClamp, 256.0f);
		FloatParameterSlider parameterLowClamp = new FloatParameterSlider("Low clamp", 0.0f, 1.0f, lowClamp, 256.0f);
		
		try {
			parameterHighClamp.addToGrid(pane, 0);
			parameterLowClamp.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			highClamp = parameterHighClamp.getValue();
			lowClamp = parameterLowClamp.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("lowclamp", String.valueOf(lowClamp)));
		paramenterElements.add(new Element("highclamp", String.valueOf(highClamp)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("lowclamp")) {
				lowClamp = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("highclamp")) {
				highClamp = Float.parseFloat(e.content);
			}
		}
	}

}
