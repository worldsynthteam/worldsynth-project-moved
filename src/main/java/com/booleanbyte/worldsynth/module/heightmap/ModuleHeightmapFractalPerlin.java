package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.common.Commons;
import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeMultitype;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.LongParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapFractalPerlin extends AbstractModule {
	
	private long seed;
	private double scale = 100.0;
	private double amplitude = 1.0;
	private double offset = 0.0;
	private Shape shape = Shape.STANDARD;
	private int octaves = 4;
	private double persistence = 0.5;
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[] dp = new int[permutationSize*2];
	
	public ModuleHeightmapFractalPerlin() {
		
		seed = new Random().nextLong();
		
		int[] p = createPermutatationTable(permutationSize, seed);
		for(int pi = 0; pi < dp.length; pi++) {
			dp[pi] = p[pi%permutationSize];
		}
	}
	
	private int[] createPermutatationTable(int size, long seed) {
		//Create a random generator with supplied seed
		Random r = new Random(seed);
		
		//Generate a list containing every integer from 0 inclusive to size exlusive
		ArrayList<Integer> valueTabel = new ArrayList<Integer>();
		for(int i = 0; i < size; i++) {
			valueTabel.add(i);
		}
		
		//create the permutation table
		int[] permutationTable = new int[size];
		
		//Insert the values from the valueTable into the permutation table in a random order
		int pi = 0;
		while(valueTabel.size() > 0) {
			int index = r.nextInt(valueTabel.size());
			permutationTable[pi] = valueTabel.get(index);
			valueTabel.remove(index);
			pi++;
		}
		
		return permutationTable;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in scale
		double scale = this.scale;
		if(inputs.get("scale") != null) {
			scale = ((DatatypeScalar)inputs.get("scale")).data;
		}
		
		//Read in amplitude and persistence
		float amplitudeValue = (float) this.amplitude;
		float persistenceValue = (float) this.persistence;
		float offsetValue = (float) this.offset;
		boolean useMaps = false;
		float[][] amplitudeMap = null;
		float[][] persistenceMap = null;
		float[][] offsetMap = null;
		if(inputs.get("amplitude") != null || inputs.get("persistence") != null || inputs.get("offset") != null) {
			if(inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitudeValue = (float) ((DatatypeScalar)inputs.get("amplitude")).data;
			}
			if(inputs.get("persistence") instanceof DatatypeScalar) {
				persistenceValue = (float) ((DatatypeScalar)inputs.get("persistence")).data;
			}
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = (float) ((DatatypeScalar)inputs.get("offset")).data / 256.0f;
			}
			
			if(inputs.get("amplitude") instanceof DatatypeHeightmap || inputs.get("persistence") instanceof DatatypeHeightmap || inputs.get("offset") instanceof DatatypeHeightmap) {
				//Use maps
				useMaps = true;
				//Amplitude
				if(inputs.get("amplitude") instanceof DatatypeHeightmap) {
					amplitudeMap = ((DatatypeHeightmap)inputs.get("amplitude")).heightMap;
				}
				else {
					amplitudeMap = Commons.fill(new float[mpw][mpl], 1.0f);
				}
				
				//Persistence
				if(inputs.get("persistence") instanceof DatatypeHeightmap) {
					persistenceMap = ((DatatypeHeightmap)inputs.get("persistence")).heightMap;
				}
				else {
					persistenceMap = Commons.fill(new float[mpw][mpl], 1.0f);
				}
				
				//Offset
				if(inputs.get("offset") instanceof DatatypeHeightmap) {
					offsetMap = ((DatatypeHeightmap)inputs.get("offset")).heightMap;
				}
				else {
					offsetMap = Commons.fill(new float[mpw][mpl], 1.5f);
				}
			}
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap)inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		if(useMaps) {
			//Use maps
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeMap[u][v] * amplitudeValue, (offsetMap[u][v]-0.5) * offsetValue, this.octaves, persistenceMap[u][v] * persistenceValue);
				}
			}
		}
		else {
			//Use values
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitudeValue, offsetValue, this.octaves, persistenceValue);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v];
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("persistence", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput(3), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(4), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset, int octaves, double persistence) {
		float height = 0;
		for(int i = 0; i < octaves; i++) {
			double m = Math.pow(persistence, i);
			height += perlin(x/scale*Math.pow(2, i), y/scale*Math.pow(2, i))*m;
		}
		
		if(persistence == 1) {
			persistence -= 0.00001;
		}
		
		switch (shape) {
		case STANDARD:
			height /= 1 + Math.pow(persistence, octaves - 1);
			height *= amplitude;
			height += 0.5f;
			height += offset;
			break;
		case RIDGED:
			double f = 1.0;
			if(octaves > 0) {
				f = (1 - Math.pow(persistence, octaves)) / (1 - persistence);
			}
			height /= f;
			height *= amplitude;
			height += offset;
			break;
		case BOWLY:
			double g = 1.0;
			if(octaves > 0) {
				g = (1 - Math.pow(persistence, octaves)) / (1 - persistence);
			}
			height /= g;
			height *= amplitude;
			height += offset;
			break;
		}
		
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	private double perlin(double x, double y) {
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		
		int aa, ab, ba, bb;
		aa = dp[dp[xi     ]+yi     ];
		ab = dp[dp[xi     ]+inc(yi)];
		ba = dp[dp[inc(xi)]+yi     ];
		bb = dp[dp[inc(xi)]+inc(yi)];
		
		double a1, a2, a3, a4;
		a1 = grad(aa, xf  , yf  , 0);
		a2 = grad(ba, xf-1, yf, 0);
		a3 = grad(ab, xf, yf-1, 0);
		a4 = grad(bb, xf-1, yf-1, 0);
		
		double a12, a34;
		a12 = lerp(a2, a1, u);
		a34 = lerp(a4, a3, u);
		double height = lerp(a34, a12, v);
		
		switch (shape) {
		case STANDARD:
			height /= 2;
			break;
		case RIDGED:
			height = 1.0f - Math.abs(height);
			break;
		case BOWLY:
			height = Math.abs(height);
			break;
		}
		
		return height;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double grad(int hash, double x, double y, double z) {
		switch(hash & 0xF)
	    {
	        case 0x0: return  x + y;
	        case 0x1: return -x + y;
	        case 0x2: return  x - y;
	        case 0x3: return -x - y;
	        case 0x4: return  x + z;
	        case 0x5: return -x + z;
	        case 0x6: return  x - z;
	        case 0x7: return -x - z;
	        case 0x8: return  y + z;
	        case 0x9: return -y + z;
	        case 0xA: return  y - z;
	        case 0xB: return -y - z;
	        case 0xC: return  y + x;
	        case 0xD: return -y + z;
	        case 0xE: return  y - x;
	        case 0xF: return -y - z;
	        default: return 0; // never happens
	    }
	}
	
	private double lerp(double a, double b, double x) {
	    return a*x + b*(1-x);
	}

	@Override
	public String getModuleName() {
		return "Fractal perlin";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Persistence"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
		};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		IntegerParameterSlider parameterOctaves = new IntegerParameterSlider("Octaves", 1, 10, octaves);
		DoubleParameterSlider parameterPersistence = new DoubleParameterSlider("Persistence", 0, 1, persistence);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		EnumParameterDropdownSelector<Shape> parameterShape = new EnumParameterDropdownSelector<Shape>("Shape", Shape.class, shape);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterAmplitude.addToGrid(pane, 1);
			parameterOffset.addToGrid(pane, 2);
			parameterOctaves.addToGrid(pane, 3);
			parameterPersistence.addToGrid(pane, 4);
			parameterSeed.addToGrid(pane, 5);
			parameterShape.addToGrid(pane, 6);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			shape = parameterShape.getValue();
			octaves = parameterOctaves.getValue();
			persistence = parameterPersistence.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			int[] p = createPermutatationTable(permutationSize, seed);
			for(int pi = 0; pi < dp.length; pi++) {
				dp[pi] = p[pi%permutationSize];
			}
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("shape", shape.name()));
		paramenterElements.add(new Element("octaves", String.valueOf(octaves)));
		paramenterElements.add(new Element("persistence", String.valueOf(persistence)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				int[] p = createPermutatationTable(permutationSize, seed);
				for(int pi = 0; pi < dp.length; pi++) {
					dp[pi] = p[pi%permutationSize];
				}
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("octaves")) {
				octaves = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("persistence")) {
				persistence = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("shape")) {
				for(Shape type: Shape.values()) {
					if(e.content.equals(type.name())) {
						shape = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Shape {
		STANDARD, RIDGED, BOWLY;
	}
}
