package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapGradient extends AbstractModule {
	
	private double scale = 100;
	private double direction = 0;
	
	private GradientTiling tiling = GradientTiling.NONE;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in scale
		double scaleValue = this.scale;
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in direction
		double directionValue = this.direction;
		if(inputs.get("direction") != null) {
			directionValue = ((DatatypeScalar) inputs.get("direction")).data;
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = (float) getHeightAt(x+u*res, z+v*res, scaleValue, directionValue);
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				map[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v];
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}
	
	public double getHeightAt(double x, double y, double scale, double direction) {
		double directionRadians = Math.toRadians(direction);
		if(direction != 0.0) {
			x = x * Math.cos(directionRadians) - y * Math.sin(directionRadians);
		}
		return gradient(x, scale);
	}
	
	private double gradient(double x, double scale) {
		double h = 0;
		h = x/scale;
		if(tiling == GradientTiling.NONE) {
			h = Math.min(Math.max(h, 0), 1);
		}
		else if(tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if(tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if(h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("direction", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeScalar(), "Direction"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterDirection = new DoubleParameterSlider("Direction", 0.0, 360.0, direction);
		EnumParameterDropdownSelector<GradientTiling> parameterTiling = new EnumParameterDropdownSelector<GradientTiling>("Tiling", GradientTiling.class, tiling);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterDirection.addToGrid(pane, 1);
			parameterTiling.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			direction = parameterDirection.getValue();
			tiling = parameterTiling.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("direction", String.valueOf(direction)));
		paramenterElements.add(new Element("tiling", tiling.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			//TODO A1.2.0 remove rotation tag
			else if(e.tag.equals("direction") || e.tag.equals("rotation")) {
				direction = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("tiling")) {
				for(GradientTiling type: GradientTiling.values()) {
					if(e.content.equals(type.name())) {
						tiling = type;
						break;
					}
				}
			}
		}
	}
	
	private enum GradientTiling {
		NONE, TILING, CONTINOUS;
	}
}
