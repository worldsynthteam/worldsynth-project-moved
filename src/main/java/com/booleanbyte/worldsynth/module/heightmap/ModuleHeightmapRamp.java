package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapRamp extends AbstractModule {
	
	private RampTiling rampType = RampTiling.STANDARD;
	private double rampFrequency = 1.0;
	private boolean normalizeScale = false;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		else if(isBypassed()) {
			return inputs.get("input");
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).heightMap;
		
		//Read in frequency
		double frequencyValue = this.rampFrequency;
		if(inputs.get("frequency") != null) {
			frequencyValue = ((DatatypeScalar) inputs.get("frequency")).data;
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = ramp(inputMap[u][v], frequencyValue, rampType);
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				outputMap[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					outputMap[u][v] = outputMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.heightMap = outputMap;
		
		return requestData;
	}
	
	private float ramp(float height, double frequency, RampTiling method) {
		double period = 1.0/frequency;
		
		double h = height/period;
		double i = Math.floor(h);
		h = h-i;
		if(method == RampTiling.CONTINOUS && i%2 != 0) {
			h = 1-h;
		}
		if(!normalizeScale) {
			h *= period;
		}
		
		return (float) h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("frequency", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Ramp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "Frequency"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		EnumParameterDropdownSelector<RampTiling> parameterRampTiling = new EnumParameterDropdownSelector<RampTiling>("Ramp type", RampTiling.class, rampType);
		DoubleParameterSlider parameterRampFrequency = new DoubleParameterSlider("Ramp frequency", 1.0, 10.0, rampFrequency);
		BooleanParameterCheckbox parameterNormalize = new BooleanParameterCheckbox("Normalize scale", normalizeScale);
		
		try {
			parameterRampTiling.addToGrid(pane, 0);
			parameterRampFrequency.addToGrid(pane, 1);
			parameterNormalize.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			rampType = parameterRampTiling.getValue();
			rampFrequency = parameterRampFrequency.getValue();
			normalizeScale = parameterNormalize.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("tiling", String.valueOf(rampType.name())));
		paramenterElements.add(new Element("frequency", String.valueOf(rampFrequency)));
		paramenterElements.add(new Element("normalize", String.valueOf(normalizeScale)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("tiling")) {
				for(RampTiling type: RampTiling.values()) {
					if(e.content.equals(type.name())) {
						rampType = type;
						break;
					}
				}
			}
			else if(e.tag.equals("frequency")) {
				rampFrequency = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("normalize")) {
				normalizeScale = Boolean.parseBoolean(e.content);
			}
		}
	}
	
	private enum RampTiling {
		STANDARD, CONTINOUS;
	}
}
