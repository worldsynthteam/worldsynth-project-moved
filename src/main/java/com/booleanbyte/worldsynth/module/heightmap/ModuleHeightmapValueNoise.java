package com.booleanbyte.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeMultitype;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.LongParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleHeightmapValueNoise extends AbstractModule {
	long seed;
	double scale = 100;
	double amplitude = 1;
	double offset = 0;
	
	int permutationSize = 255;
	int repeat = permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	int[] dp = new int[permutationSize*2];
	
	public ModuleHeightmapValueNoise() {
		
		seed = new Random().nextLong();
		
		int[] p = createPermutatationTable(permutationSize, seed);
		for(int pi = 0; pi < dp.length; pi++) {
			dp[pi] = p[pi%permutationSize];
		}
	}
	
	private int[] createPermutatationTable(int size, long seed) {
		//Create a random generator with supplied seed
		Random r = new Random(seed);
		
		//Generate a list containing every integer from 0 inclusive to size exlusive
		ArrayList<Integer> valueTabel = new ArrayList<Integer>();
		for(int i = 0; i < size; i++) {
			valueTabel.add(i);
		}
		
		//create the permutation table
		int[] permutationTable = new int[size];
		
		//Insert the values from the valueTable into the permutation table in a random order
		int pi = 0;
		while(valueTabel.size() > 0) {
			int index = r.nextInt(valueTabel.size());
			permutationTable[pi] = valueTabel.get(index);
			valueTabel.remove(index);
			pi++;
		}
		
		return permutationTable;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in scale
		double scaleValue = this.scale;
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in amplitude
		double amplitudeValue = this.amplitude;
		float[][] amplitudeMap = null;
		if(inputs.get("amplitude") != null) {
			if(inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitudeValue = ((DatatypeScalar) inputs.get("amplitude")).data;
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).heightMap;
			}
		}
		
		//Read in offset
		double offsetValue = this.offset;
		float[][] offsetMap = null;
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = ((DatatypeScalar) inputs.get("offset")).data / 256.0;
			}
			else {
				offsetMap = ((DatatypeHeightmap)inputs.get("offset")).heightMap;
			}
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).heightMap;
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has both amplitude and offset map
		if(amplitudeMap != null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeMap[u][v] * amplitudeValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has amplitude map
		else if(amplitudeMap != null && offsetMap == null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeMap[u][v] * amplitudeValue, offsetValue);
				}
			}
		}
		//Has offset map
		else if(amplitudeMap == null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeValue, offsetValue);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v];
				}
			}
		}
		
		requestData.heightMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset) {
		return valueNoise(x/scale, y/scale, amplitude, offset);
	}
	
	private double valueNoise(double x, double y, double amplitude, double offset) {
		
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		
		int aa, ab, ba, bb;
		aa = hash(xi, yi);
		ab = hash(xi, inc(yi));
		ba = hash(inc(xi), yi);
		bb = hash(inc(xi), inc(yi));
		
		double a12, a34;
		a12 = lerp(ba, aa, u);
		a34 = lerp(bb, ab, u);
		double height = lerp(a34, a12, v)/256;
		height *= amplitude;
		height += offset;
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	private int hash(int x, int y) {
		int h = dp[dp[x]+y];
		return h;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double lerp(double a, double b, double x) {
	    return a*x + b*(1-x);
	}

	@Override
	public String getModuleName() {
		return "Value noise";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] { new DatatypeScalar(), new DatatypeHeightmap() }), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] { new DatatypeScalar(), new DatatypeHeightmap() }), "Offset"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterAmplitude.addToGrid(pane, 1);
			parameterOffset.addToGrid(pane, 2);
			parameterSeed.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			int[] p = createPermutatationTable(permutationSize, seed);
			for(int pi = 0; pi < dp.length; pi++) {
				dp[pi] = p[pi%permutationSize];
			}
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				int[] p = createPermutatationTable(permutationSize, seed);
				for(int pi = 0; pi < dp.length; pi++) {
					dp[pi] = p[pi%permutationSize];
				}
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
		}
	}
}
