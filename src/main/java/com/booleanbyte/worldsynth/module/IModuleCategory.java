package com.booleanbyte.worldsynth.module;

import javafx.scene.paint.Color;

public interface IModuleCategory {
	Color classColor();
}
