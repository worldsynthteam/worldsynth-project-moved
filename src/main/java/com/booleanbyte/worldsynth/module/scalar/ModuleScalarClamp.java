package com.booleanbyte.worldsynth.module.scalar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleScalarClamp extends AbstractModule {
	
	double lowClamp = 0.0;
	double highClamp = 1.0;

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeScalar requestData = (DatatypeScalar) request.data;
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		if(inputs.get("high") != null) {
			highClamp = ((DatatypeScalar) inputs.get("high")).data;
		}
		if(inputs.get("low") != null) {
			lowClamp = ((DatatypeScalar) inputs.get("low")).data;
		}
		
		double i0 = ((DatatypeScalar) inputs.get("input")).data;
		double o = clamp(i0);
		
		requestData.data = o;
		
		return requestData;
	}
	
	private double clamp(double height) {
		if(height > highClamp) height = highClamp;
		else if(height < lowClamp) height = lowClamp;
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), outputRequest.data));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Scalar clamp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_SCALAR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "High clamp"),
				new ModuleInput(new DatatypeScalar(), "Low clamp")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeScalar(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterHighClamp = new DoubleParameterSlider("High clamp", 0.0, 1.0, highClamp);
		DoubleParameterSlider parameterLowClamp = new DoubleParameterSlider("Low clamp", 0.0, 1.0, lowClamp);
		
		try {
			parameterHighClamp.addToGrid(pane, 0);
			parameterLowClamp.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			highClamp = parameterHighClamp.getValue();
			lowClamp = parameterLowClamp.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("lowclamp", String.valueOf(lowClamp)));
		paramenterElements.add(new Element("highclamp", String.valueOf(highClamp)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("lowclamp")) {
				lowClamp = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("highclamp")) {
				highClamp = Double.parseDouble(e.content);
			}
		}
	}

}
