package com.booleanbyte.worldsynth.module.colormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeColormap;
import com.booleanbyte.worldsynth.datatype.DatatypeScalar;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleColormapTranslate extends AbstractModule {
	
	double xTranslate = 0;
	double zTranslate = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double xt = xTranslate;
		double zt = zTranslate;
		
		DatatypeScalar scalarDataX = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		DatatypeScalar scalarDataZ = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput(2), new DatatypeScalar()));
		
		if(scalarDataX != null) {
			xt = scalarDataX.data;
		}
		if(scalarDataZ != null) {
			zt = scalarDataZ.data;
		}
		
		DatatypeColormap requestData = (DatatypeColormap) outputRequest.data;
		DatatypeColormap translatedRequestData = new DatatypeColormap(requestData.x-xt, requestData.z-zt, requestData.width, requestData.length, requestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), translatedRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Translate";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "xTranslate"),
				new ModuleInput(new DatatypeScalar(), "zTranslate")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		DoubleParameterSlider parameterXTranslate = new DoubleParameterSlider("Translate x", -100, 100, xTranslate);
		DoubleParameterSlider parameterZTranslate = new DoubleParameterSlider("Translate z", -100, 100, zTranslate);
		
		try {
			parameterXTranslate.addToGrid(pane, 0);
			parameterZTranslate.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			xTranslate = parameterXTranslate.getValue();
			zTranslate = parameterZTranslate.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("xtranslate", String.valueOf(xTranslate)));
		paramenterElements.add(new Element("ztranslate", String.valueOf(zTranslate)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("xtranslate")) {
				xTranslate = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("ztranslate")) {
				zTranslate = Double.parseDouble(e.content);
			}
		}
	}
}
