package com.booleanbyte.worldsynth.module.colormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.common.color.WsColor;
import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeColormap;
import com.booleanbyte.worldsynth.datatype.DatatypeMaterialmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.material.MaterialRegistry;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleColormapFromMaterialmap extends AbstractModule {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if(inputs.get("input") == null) {
			//If the input is null, there is not enough input and then just return null
			return null;
		}
		
		int[][] inputMaterialmap0 = ((DatatypeMaterialmap) inputs.get("input")).materialMap;
		
		float[][][] map = new float[mpw][mpl][3];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				WsColor c = MaterialRegistry.getMaterialByInternalId(inputMaterialmap0[u][v]).getWsColor();
				float red = (float) c.getRed();
				float green = (float) c.getGreen();
				float blue = (float) c.getBlue();
				map[u][v][0] = red;
				map[u][v][1] = green;
				map[u][v][2] = blue;
			}
		}
		
		requestData.colorMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		
		DatatypeMaterialmap materialmapRequestData = new DatatypeMaterialmap(colormapRequestData.x, colormapRequestData.z, colormapRequestData.width, colormapRequestData.length, colormapRequestData.resolution);

		inputRequests.put("input", new ModuleInputRequest(getInput(0), materialmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap from materialmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeMaterialmap(), "Primary input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
	}
}
