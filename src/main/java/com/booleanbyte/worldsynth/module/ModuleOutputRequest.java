package com.booleanbyte.worldsynth.module;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;

/**
 * Used in the process of building a node tree, to pass a request for data from a module.
 * This is the request received by a module for it to determine what data to build and what
 * inputs it needs to build, so it can construct the corresponding inputrequests to send out.
 */
public class ModuleOutputRequest {
	public ModuleOutput output;
	public AbstractDatatype data;
	
	public ModuleOutputRequest(ModuleOutput output, AbstractDatatype data) {
		this.output = output;
		this.data = data;
	}
}
