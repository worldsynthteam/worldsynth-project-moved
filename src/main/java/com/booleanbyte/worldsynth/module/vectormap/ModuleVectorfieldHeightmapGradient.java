package com.booleanbyte.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeVectormap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleVectorfieldHeightmapGradient extends AbstractModule {
	
	double gain = 1.0f;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		float res = (float) requestData.resolution;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		float[][][] field = new float[mpw][mpl][2];
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("input")).heightMap;
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float[] gradient = getGradient(inputMap0, u+1, v+1, res);
				
				gradient[0] *= gain;
				gradient[1] *= gain;
				
				field[u][v] = gradient;
			}
		}
		
		requestData.vectorField = field;
		
		return requestData;
	}
	
	private float[] getGradient(float[][] heightmap, int x, int y, float res) {
		
		float cph = getHeight(heightmap, x, y);
		float dnx = cph - getHeight(heightmap, x-1, y  );
		float dpx = cph - getHeight(heightmap, x+1, y  );
		float dny = cph - getHeight(heightmap, x  , y-1);
		float dpy = cph - getHeight(heightmap, x  , y+1);
		
		float dx = (dnx - dpx) / (2*res);
		float dy = (dny - dpy) / (2*res);
		
		dx *= 100;
		dy *= 100;
		
		float[] gradient = {dx, dy};
		
		return gradient;
	}
	
	private float getHeight(float[][] heightmap, int x, int y) {
		if(x < 0) {
			x = 0;
		}
		else if(x >= heightmap.length) {
			x = heightmap.length-1;
		}
		
		if(y < 0) {
			y = 0;
		}
		else if(y >= heightmap[0].length) {
			y = heightmap[0].length-1;
		}
		
		float height = heightmap[(int)x][(int)y];
		
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap vectorfieldRequestData = (DatatypeVectormap) outputRequest.data;
		double res = vectorfieldRequestData.resolution;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vectorfieldRequestData.x-res, vectorfieldRequestData.z-res, vectorfieldRequestData.width+2*res, vectorfieldRequestData.length+2*res, vectorfieldRequestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient vectorfield";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VECTORFIELD;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {new ModuleInput(new DatatypeHeightmap(), "Primary input")};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeVectormap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterGain = new DoubleParameterSlider("Gain", 0, 2, gain);
		
		try {
			parameterGain.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gain = parameterGain.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("gain", String.valueOf(gain)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("gain")) {
				gain = Double.parseDouble(e.content);
			}
		}
	}

}
