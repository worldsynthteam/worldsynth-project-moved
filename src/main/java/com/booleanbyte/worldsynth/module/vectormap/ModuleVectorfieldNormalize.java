package com.booleanbyte.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeVectormap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleVectorfieldNormalize extends AbstractModule {
	
	double gain = 1.0f;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		float[][][] field = new float[mpw][mpl][2];
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		
		float[][][] inputField0 = ((DatatypeVectormap) inputs.get("input")).vectorField;
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				field[u][v] = normalize(inputField0[u][v]);
			}
		}
		
		requestData.vectorField = field;
		
		return requestData;
	}
	
	private float[] normalize(float[] vector) {
		float lenght = (float)Math.sqrt(vector[0]*vector[0]+vector[1]*vector[1]);
		float[] normalizedVector = {vector[0]/lenght, vector[1]/lenght};
		
		return normalizedVector;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap vectorfieldRequestData = (DatatypeVectormap) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), vectorfieldRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Normalize";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_VECTORFIELD;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {new ModuleInput(new DatatypeVectormap(), "Primary input")};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeVectormap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
	}

}
