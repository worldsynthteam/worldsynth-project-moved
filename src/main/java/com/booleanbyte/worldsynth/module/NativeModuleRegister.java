package com.booleanbyte.worldsynth.module;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import com.booleanbyte.worldsynth.module.biomemap.ModuleBiomemapConstant;
import com.booleanbyte.worldsynth.module.biomemap.ModuleBiomemapParameterVoronoi;
import com.booleanbyte.worldsynth.module.biomemap.ModuleBiomemapSelector;
import com.booleanbyte.worldsynth.module.biomemap.ModuleBiomemapTranslate;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceCombiner;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceFilter;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceFromHeightmap;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceFromValuespace;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceHeightClamp;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspacePerlinWorm;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceSurfaceCover;
import com.booleanbyte.worldsynth.module.blockspace.ModuleBlockspaceTranslate;
import com.booleanbyte.worldsynth.module.colormap.ModuleColormapConstant;
import com.booleanbyte.worldsynth.module.colormap.ModuleColormapFromMaterialmap;
import com.booleanbyte.worldsynth.module.colormap.ModuleColormapGradient;
import com.booleanbyte.worldsynth.module.colormap.ModuleColormapSelector;
import com.booleanbyte.worldsynth.module.colormap.ModuleColormapTranslate;
import com.booleanbyte.worldsynth.module.featuremap.ModuleFeaturemapSimpleDistribution;
import com.booleanbyte.worldsynth.module.featurespace.ModuleFeaturespaceSimpleDistribution;
import com.booleanbyte.worldsynth.module.fileio.ModuleColormapExporter;
import com.booleanbyte.worldsynth.module.fileio.ModuleColormapImporter;
import com.booleanbyte.worldsynth.module.fileio.ModuleHeightmapExporter;
import com.booleanbyte.worldsynth.module.fileio.ModuleHeightmapImporter;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapCells;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapClamp;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapCombiner;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapConstant;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapExpander;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapExtentGradient;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapFractalPerlin;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapFromBlockspace;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapGain;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapGradient;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapHeightSelector;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapInverter;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapMacSearlasPseudoErosion;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapRamp;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapSimplePerlin;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapSlope;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapSmothen;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapTerrace;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapTranslate;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapValueNoise;
import com.booleanbyte.worldsynth.module.heightmap.ModuleHeightmapWorley;
import com.booleanbyte.worldsynth.module.heightmapoverlay.ModuleOverlayGenerator;
import com.booleanbyte.worldsynth.module.macro.ModuleHeightmapMacroEntry;
import com.booleanbyte.worldsynth.module.macro.ModuleHeightmapMacroExit;
import com.booleanbyte.worldsynth.module.materialmap.ModuleMaterialmapConstant;
import com.booleanbyte.worldsynth.module.materialmap.ModuleMaterialmapFromBiomemap;
import com.booleanbyte.worldsynth.module.materialmap.ModuleMaterialmapFromColormap;
import com.booleanbyte.worldsynth.module.objects.ModuleObjectPlacer;
import com.booleanbyte.worldsynth.module.objects.ModuleObjectsImporter;
import com.booleanbyte.worldsynth.module.scalar.ModuleScalarClamp;
import com.booleanbyte.worldsynth.module.scalar.ModuleScalarCombiner;
import com.booleanbyte.worldsynth.module.scalar.ModuleScalarConstat;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespaceCombiner;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespaceGradient;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespacePointDistance;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespaceSimplePerlin;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespaceTranslate;
import com.booleanbyte.worldsynth.module.valuespace.ModuleValuespaceWorley;
import com.booleanbyte.worldsynth.module.vectormap.ModuleVectorfieldHeightmapGradient;
import com.booleanbyte.worldsynth.module.vectormap.ModuleVectorfieldNormalize;
import com.booleanbyte.worldsynth.module.vectormap.ModuleVectorfieldTranslate;

public class NativeModuleRegister extends AbstractModuleRegister {
	
	public NativeModuleRegister(File worldSynthConfigDirectory) {
		super();
		
		try {
			//Scalars
			registerModule(ModuleScalarConstat.class, "\\Scalar");
			
			registerModule(ModuleScalarClamp.class, "\\Scalar");
			
			registerModule(ModuleScalarCombiner.class, "\\Scalar");
			
			
			//Heightmap
			registerModule(ModuleHeightmapConstant.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapGradient.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapSimplePerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFractalPerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapValueNoise.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapWorley.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapCells.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFromBlockspace.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapExtentGradient.class, "\\Heightmap\\Generator");
			
			registerModule(ModuleHeightmapGain.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTerrace.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapInverter.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapRamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSmothen.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapClamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapMacSearlasPseudoErosion.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapExpander.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSlope.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTranslate.class, "\\Heightmap\\Modifier");
			
			registerModule(ModuleHeightmapCombiner.class, "\\Heightmap");
//			registerModule(ModuleHeightmapSelector.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapHeightSelector.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapExporter.class, "\\Heightmap");
			registerModule(ModuleHeightmapImporter.class, "\\Heightmap");
			
			
			//Biomemap
			registerModule(ModuleBiomemapConstant.class, "\\Biomemap\\Generator");
			registerModule(ModuleBiomemapParameterVoronoi.class, "\\Biomemap\\Generator");
			
			registerModule(ModuleBiomemapTranslate.class, "\\Biomemap\\Modifier");
			
			registerModule(ModuleBiomemapSelector.class, "\\Biomemap");
			
			
			//Valuespace
			registerModule(ModuleValuespaceSimplePerlin.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceWorley.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespacePointDistance.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceGradient.class, "\\Valuespace\\Generator");
//			registerModule(ModuleValuespaceProjectedDensity.class, "\\Valuespace\\Generator");
			
			registerModule(ModuleValuespaceTranslate.class, "\\Valuespace\\Modifier");
			
			registerModule(ModuleValuespaceCombiner.class, "\\Valuespace");
			
			
			//Blockspace
			registerModule(ModuleBlockspaceFromValuespace.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspaceFromHeightmap.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspacePerlinWorm.class, "\\Blockspace\\Generator");
			
			registerModule(ModuleBlockspaceHeightClamp.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceTranslate.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceSurfaceCover.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceFilter.class, "\\Blockspace\\Modifier");
			
			registerModule(ModuleBlockspaceCombiner.class, "\\Blockspace");
			
			
			//Colormap
			registerModule(ModuleColormapConstant.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapGradient.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapFromMaterialmap.class, "\\Colormap\\Generator");
			
			registerModule(ModuleColormapTranslate.class, "\\Colormap\\Modifier");
			
			registerModule(ModuleColormapSelector.class, "\\Colormap");
			
			registerModule(ModuleColormapExporter.class, "\\Colormap");
			registerModule(ModuleColormapImporter.class, "\\Colormap");
			
			
			//Vectorfield
			registerModule(ModuleVectorfieldHeightmapGradient.class, "\\Vectormap\\Generator");
			
			registerModule(ModuleVectorfieldNormalize.class, "\\Vectormap\\Modifier");
			registerModule(ModuleVectorfieldTranslate.class, "\\Vectormap\\Modifier"); 
			
			
			//Heightmap overlay
			registerModule(ModuleOverlayGenerator.class, "\\Overlay");
			
			
			//Materialmap
			registerModule(ModuleMaterialmapConstant.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromColormap.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromBiomemap.class, "\\Materialmap\\Generator");
			
			//Featuremap
			registerModule(ModuleFeaturemapSimpleDistribution.class, "\\Featuremap\\Generator");
			
			//Featurespace
			registerModule(ModuleFeaturespaceSimpleDistribution.class, "\\Featurespace\\Generator");
			
			//Objects
			registerModule(ModuleObjectPlacer.class, "\\Objects");
			registerModule(ModuleObjectsImporter.class, "\\Objects");
			
			//Macro
			registerModule(ModuleMacro.class, "\\Macro");
			registerModule(ModuleHeightmapMacroEntry.class, "\\Macro\\IO");
			registerModule(ModuleHeightmapMacroExit.class, "\\Macro\\IO");
			
			
			//Minecraft
//			registerModule(ModuleMinecraftWorldExport.class, "\\Minecraft save");
//			registerModule(ModuleMinecraftWorldImport.class, "\\Minecraft save");
			

			//Expperimental
//			registerModule(ModuleHeightmapSimpleErosion.class, "\\Erosion");
			
			
			//Load external libs
			File libFolder = new File(worldSynthConfigDirectory, "lib");
			if(!libFolder.exists()) {
				libFolder.mkdir();
			}
			loadeExternalLib(libFolder);
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
	
	private void loadeExternalLib(File libDirectory) {
		
		if(!libDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + libDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: libDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadeExternalLib(sub);
			}
			else if(sub.getAbsolutePath().endsWith(".jar")) {
				try {
					ModuleRegisterLoader loader = new ModuleRegisterLoader(sub);
					for(AbstractModuleRegister libRegister: loader.getModuleRegisters()) {
						for(ModuleEntry moduleEntry: libRegister.getRegisteredModuleEntries()) {
							try {
								registerModule(moduleEntry);
							} catch (ClassNotModuleExeption e) {
								e.printStackTrace();
							}
						}
					}
				} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
						| IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
