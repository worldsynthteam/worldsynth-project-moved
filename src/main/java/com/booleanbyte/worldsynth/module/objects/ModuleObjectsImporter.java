package com.booleanbyte.worldsynth.module.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.booleanbyte.worldsynth.customobject.CustomObject;
import com.booleanbyte.worldsynth.customobject.LocatedCustomObject;
import com.booleanbyte.worldsynth.customobject.file.Bo2CustomObject;
import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeObjects;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class ModuleObjectsImporter extends AbstractModule {
	
	File[] objectFiles = new File[0];
	CustomObject[] cachedObjects;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		if(objectFiles == null) {
			return null;
		}
		else if(objectFiles.length == 0) {
			return null;
		}
		
		//Check if the object files are cached, if not read them from file to cache
		if(cachedObjects == null) {
			cachedObjects = new CustomObject[objectFiles.length];
			for(int i = 0; i < objectFiles.length; i++) {
				File objectFile = objectFiles[i];
				if(!objectFile.getAbsolutePath().toLowerCase().endsWith(".bo2")) {
					return null;
				}
				
				try {
					cachedObjects[i] = new Bo2CustomObject(objectFile);
				} catch (IOException e) {
					e.printStackTrace();
					cachedObjects = null;
					return null;
				}
			}
		}
		
		//Clone objects for return value based on seed
		for(LocatedCustomObject locatedObject: requestData.getLocatedObjects()) {
			locatedObject.setObject(cachedObjects[(int) (locatedObject.getSeed() % cachedObjects.length)]);
		}
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Object importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_OBJECT;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ListView<File> objectListView = new ListView<>(FXCollections.observableArrayList(Arrays.asList(objectFiles)));
		objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		objectListView.setPrefSize(600, 400);
		
		GridPane.setColumnSpan(objectListView, 3);
		pane.add(objectListView, 0, 0);
		
		Button addObjectsButton = new Button("Add");
		Button removeObjectsButton = new Button("Remove");
		
		addObjectsButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new ExtensionFilter("Object", "*.BO2"));
			List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
			if(selectedFiles != null) {
				objectListView.getItems().addAll(selectedFiles);
			}
		});
		
		removeObjectsButton.setOnAction(e -> {
			objectListView.getItems().removeAll(objectListView.getSelectionModel().getSelectedItems());
		});
		
		pane.add(addObjectsButton, 0, 1);
		pane.add(removeObjectsButton, 1, 1);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			objectFiles = objectListView.getItems().toArray(new File[0]);
			cachedObjects = null;
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		String filePathStringList = "";
		boolean firstEntry = true;
		for(File f: objectFiles) {
			if(firstEntry) {
				firstEntry = false;
			}
			else {
				filePathStringList += ";";
			}
			filePathStringList += f.getAbsolutePath();
		}
		
		paramenterElements.add(new Element("filelist", filePathStringList));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("filelist")) {
				ArrayList<File> recoveredFileEntries = new ArrayList<File>();
				String[] stringFileList = e.content.split(";");
				for(String s: stringFileList) {
					recoveredFileEntries.add(new File(s));
				}
				objectFiles = new File[recoveredFileEntries.size()];
				recoveredFileEntries.toArray(objectFiles);
				cachedObjects = null;
			}
		}
	}
}
