package com.booleanbyte.worldsynth.module.valuespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeValuespace;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.LongParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleValuespaceSimplePerlin extends AbstractModule {
	
	long seed;
	double scale = 100;
	double amplitude = 1;
	double offset = 0;
	Shape shape = Shape.STANDARD;
	
	
	int permutationSize = 256;
	int repeat = permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	int[] dp = new int[permutationSize*2];
	
	public ModuleValuespaceSimplePerlin() {
		
		seed = new Random().nextLong();
		
		int[] p = createPermutatationTable(permutationSize, seed);
		for(int pi = 0; pi < dp.length; pi++) {
			dp[pi] = p[pi%permutationSize];
		}
	}
	
	private int[] createPermutatationTable(int size, long seed) {
		//Create a random generator with supplied seed
		Random r = new Random(seed);
		
		//Generate a list containing every integer from 0 inclusive to size exlusive
		ArrayList<Integer> valueTabel = new ArrayList<Integer>();
		for(int i = 0; i < size; i++) {
			valueTabel.add(i);
		}
		
		//create the permutation table
		int[] permutationTable = new int[size];
		
		//Insert the values from the valueTable into the permutation table in a random order
		int pi = 0;
		while(valueTabel.size() > 0) {
			int index = r.nextInt(valueTabel.size());
			permutationTable[pi] = valueTabel.get(index);
			valueTabel.remove(index);
			pi++;
		}
		
		return permutationTable;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		float[][][] values = new float[spw][sph][spl];
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					values[u][v][w] = (float) getValueAt(x+u*res, y+v*res, z+w*res);
				}
			}
		}
		
		requestData.valuespace = values;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}
	
	public double getValueAt(double x, double y, double z) {
		return perlin3d(x/scale, y/scale, z/scale);
	}
	
	private double perlin3d(double x, double y, double z) {
		
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
			if(z < 0) {
				z = repeat+(z%repeat);
			}
			else {
				z = z%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		int zi = (int)z & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		double zf = z - (int)z;
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		double w = easeCurve(zf);
		
		// zxy _ a=0, b=1
		int aaa, aab, aba, abb, baa, bab, bba, bbb;
		aaa = dp[dp[dp[xi     ]+yi     ]+zi     ];
		baa = dp[dp[dp[inc(xi)]+yi     ]+zi     ];
		bba = dp[dp[dp[inc(xi)]+inc(yi)]+zi     ];
		aba = dp[dp[dp[xi     ]+inc(yi)]+zi     ];
		aab = dp[dp[dp[xi     ]+yi     ]+inc(zi)];
		bab = dp[dp[dp[inc(xi)]+yi     ]+inc(zi)];
		bbb = dp[dp[dp[inc(xi)]+inc(yi)]+inc(zi)];
		abb = dp[dp[dp[xi     ]+inc(yi)]+inc(zi)];
		
		double a1, a2, a3, a4, a5, a6, a7, a8;
		a1 = grad(aaa, xf  , yf  , zf);
		a2 = grad(baa, xf-1, yf, zf);
		a3 = grad(aba, xf, yf-1, zf);
		a4 = grad(bba, xf-1, yf-1, zf);
		a5 = grad(aab, xf  , yf  , zf-1);
		a6 = grad(bab, xf-1, yf, zf-1);
		a7 = grad(abb, xf, yf-1, zf-1);
		a8 = grad(bbb, xf-1, yf-1, zf-1);
		
		double a12, a34, a56, a78, a1234, a5678;
		a12 = lerp(a1, a2, u);
		a34 = lerp(a3, a4, u);
		a56 = lerp(a5, a6, u);
		a78 = lerp(a7, a8, u);
		
		a1234 = lerp(a12, a34, v);
		a5678 = lerp(a56, a78, v);
		
		double value = lerp(a1234, a5678, w);
		
		switch (shape) {
		case STANDARD:
			value /= 2;
			value *= amplitude;
			value += 0.5;
			value += offset;
			break;
		case RIDGED:
			value = 1.0f - Math.abs(value);
			value *= amplitude;
			value += offset;
			break;
		case BOWLY:
			value = Math.abs(value);
			value *= amplitude;
			value += offset;
			break;
		}
		
		value = Math.min(value, 1);
		value = Math.max(value, 0);
		return value;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double grad(int hash, double x, double y, double z) {
		switch(hash & 0xF)
	    {
		    case 0x0: return  x + y;
	        case 0x1: return -x + y;
	        case 0x2: return  x - y;
	        case 0x3: return -x - y;
	        case 0x4: return  x + z;
	        case 0x5: return -x + z;
	        case 0x6: return  x - z;
	        case 0x7: return -x - z;
	        case 0x8: return  y + z;
	        case 0x9: return -y + z;
	        case 0xA: return  y - z;
	        case 0xB: return -y - z;
	        case 0xC: return  y + x;
	        case 0xD: return -y + z;
	        case 0xE: return  y - x;
	        case 0xF: return -y - z;
	        default: return 0; // never happens
	    }
	}
	
	private double lerp(double a, double b, double x) {
		return a + x * (b - a);
	}

	@Override
	public String getModuleName() {
		return "Valuespace simpel perlin";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		EnumParameterDropdownSelector<Shape> parameterShape = new EnumParameterDropdownSelector<Shape>("Shape", Shape.class, shape);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterAmplitude.addToGrid(pane, 1);
			parameterOffset.addToGrid(pane, 2);
			parameterSeed.addToGrid(pane, 3);
			parameterShape.addToGrid(pane, 4);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			shape = parameterShape.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			int[] p = createPermutatationTable(permutationSize, seed);
			for(int pi = 0; pi < dp.length; pi++) {
				dp[pi] = p[pi%permutationSize];
			}
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("shape", shape.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				int[] p = createPermutatationTable(permutationSize, seed);
				for(int pi = 0; pi < dp.length; pi++) {
					dp[pi] = p[pi%permutationSize];
				}
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("shape")) {
				for(Shape type: Shape.values()) {
					if(e.content.equals(type.name())) {
						shape = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Shape {
		STANDARD, RIDGED, BOWLY;
	}
}
