package com.booleanbyte.worldsynth.module.valuespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.datatype.DatatypeValuespace;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleValuespaceGradient extends AbstractModule {
	
	double scale = 100;
	double rotation = 0;
	double tilt = 0;
	
	GradientTiling tiling = GradientTiling.NONE;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		float[][][] space = new float[spw][sph][spl];
		
		boolean useHeightmap = false;
		float[][] inputMap0 = null;
		if(inputs.get("dmap") != null) {
			useHeightmap = true;
			inputMap0 = ((DatatypeHeightmap) inputs.get("dmap")).heightMap;
		}
		
		boolean useValuespace = false;
		float[][][] inputSpace1 = null;
		if(inputs.get("dspace") != null) {
			useValuespace = true;
			inputSpace1 = ((DatatypeValuespace) inputs.get("dspace")).valuespace;
		}
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float offset = 0;
					if(useHeightmap) {
						offset += inputMap0[u][w];
					}
					if(useValuespace) {
						offset += inputSpace1[u][v][w];
					}
					
					float o = (float) getValueAt(x+u*res, y+v*res, z+w*res, offset);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					space[u][v][w] = o;
				}
			}
		}
		
		requestData.valuespace = space;
		
		return requestData;
	}
	
	public double getValueAt(double x, double y, double z, double offset) {
		double rr = Math.toRadians(-rotation);
		double tr = Math.toRadians(tilt);
		
		//Rotate around y
		if(rotation != 0.0) {
			double tx = x*cos(rr) + 0 + z*sin(rr);
			double ty = 0 + y - 0;
			//double tz = -x*sin(rr) + 0 + z*cos(rr);
			
			x = tx;
			y = ty;
			//z = tz;
		}
		//Tilt around z
		if(tilt != 0.0) {
			//double tx = x*cos(tr) - y*sin(tr) + 0;
			double ty = x*sin(tr) + y*cos(tr) + 0;
			//double tz = 0 + 0 + z;
			
			//x = tx;
			y = ty;
			//z = tz;
		}
		
		return gradient(y + offset*10.0);
	}
	
	private double cos(double a) {
		return Math.cos(a);
	}
	
	private double sin(double a) {
		return Math.sin(a);
	}
	
	private double gradient(double x) {
		double h = 0;
		h = x/scale;
		if(tiling == GradientTiling.NONE) {
			h = Math.min(Math.max(h, 0), 1);
		}
		else if(tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if(tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if(h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace valuespaceRequestData = (DatatypeValuespace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(valuespaceRequestData.x, valuespaceRequestData.z, valuespaceRequestData.width, valuespaceRequestData.lenght, valuespaceRequestData.resolution);
		
		inputRequests.put("dmap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("dspace", new ModuleInputRequest(getInput(1), valuespaceRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Displace map"),
				new ModuleInput(new DatatypeValuespace(), "Displace space")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterRotation = new DoubleParameterSlider("Rotation", 0.0, 360.0, rotation);
		DoubleParameterSlider parameterTilt = new DoubleParameterSlider("Tilt", 0.0,90.0, tilt);
		EnumParameterDropdownSelector<GradientTiling> parameterTiling = new EnumParameterDropdownSelector<GradientTiling>("Tiling", GradientTiling.class, tiling);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterRotation.addToGrid(pane, 1);
			parameterTilt.addToGrid(pane, 2);
			parameterTiling.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			rotation = parameterRotation.getValue();
			tilt = parameterTilt.getValue();
			tiling = parameterTiling.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("rotation", String.valueOf(rotation)));
		paramenterElements.add(new Element("tilt", String.valueOf(tilt)));
		paramenterElements.add(new Element("tiling", tiling.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("rotation")) {
				rotation = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("tilt")) {
				tilt = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("tiling")) {
				for(GradientTiling type: GradientTiling.values()) {
					if(e.content.equals(type.name())) {
						tiling = type;
						break;
					}
				}
			}
		}
	}
	
	private enum GradientTiling {
		NONE, TILING, CONTINOUS;
	}
}
