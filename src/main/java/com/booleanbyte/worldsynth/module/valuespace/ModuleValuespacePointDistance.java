package com.booleanbyte.worldsynth.module.valuespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeValuespace;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleValuespacePointDistance extends AbstractModule {
	
	double dist = 50;
	
	double pointX = 0;
	double pointY = 0;
	double pointZ = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.x;
		double y = requestData.y;
		double z = requestData.z;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float o = 0.0f;
					
					double gx = x + (double)u * res;
					double gy = y + (double)v * res;;
					double gz = z + (double)w * res;;
					
					o = (float) (dist(pointX, pointY, pointZ, gx, gy, gz) / dist);
					
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					valuespace[u][v][w] = o;
				}
			}
		}
		
		requestData.valuespace = valuespace;
		
		return requestData;
	}
	
	private double dist(double x1, double y1, double z1, double x2, double y2, double z2) {
		double dx = x1 - x2;
		double dy = y1 - y2;
		double dz = z1 - z2;
		
		double d = Math.sqrt(dx * dx + dy * dy + dz * dz);
		
		return d;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Valuespace pointdistance";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		DoubleParameterSlider parameterDist = new DoubleParameterSlider("Distance", 0, 200, dist);
		DoubleParameterSlider parameterPointX = new DoubleParameterSlider("Point x", -500, 500, pointX);
		DoubleParameterSlider parameterPointY = new DoubleParameterSlider("Point y", 0, 255, pointY);
		DoubleParameterSlider parameterPointZ = new DoubleParameterSlider("Point z", -500, 500, pointZ);
		
		try {
			parameterDist.addToGrid(pane, 0);
			parameterPointX.addToGrid(pane, 1);
			parameterPointY.addToGrid(pane, 2);
			parameterPointZ.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			dist = parameterDist.getValue();
			pointX = parameterPointX.getValue();
			pointY = parameterPointY.getValue();
			pointZ = parameterPointZ.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("dist", String.valueOf(dist)));
		paramenterElements.add(new Element("pointx", String.valueOf(pointX)));
		paramenterElements.add(new Element("pointy", String.valueOf(pointY)));
		paramenterElements.add(new Element("pointz", String.valueOf(pointZ)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("dist")) {
				dist = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("pointx")) {
				pointX = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("pointy")) {
				pointY = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("pointz")) {
				pointZ = Double.parseDouble(e.content);
			}
		}
	}
}
