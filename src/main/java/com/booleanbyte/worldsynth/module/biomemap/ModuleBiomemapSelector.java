package com.booleanbyte.worldsynth.module.biomemap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBiomemap;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleBiomemapSelector extends AbstractModule {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBiomemap requestData = (DatatypeBiomemap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if(!inputs.containsKey("selector") || !inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If any of the inputs are not available, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("selector")).heightMap;
		int[][] inputMap1 = ((DatatypeBiomemap) inputs.get("primary")).biomeMap;
		int[][] inputMap2 = ((DatatypeBiomemap) inputs.get("secondary")).biomeMap;
		
		int[][] map = new int[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				
				int bid = inputMap2[u][v];
				if(inputMap0[u][v] > 0) {
					bid = inputMap1[u][v];
				}
				map[u][v] = bid;
			}
		}
		
		requestData.biomeMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBiomemap biomemapRequestData = (DatatypeBiomemap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(biomemapRequestData.x, biomemapRequestData.z, biomemapRequestData.width, biomemapRequestData.length, biomemapRequestData.resolution);
		
		inputRequests.put("selector", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(1), biomemapRequestData));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(2), biomemapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_BIOMEMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Selector input"),
				new ModuleInput(new DatatypeBiomemap(), "Primary biomemap"),
				new ModuleInput(new DatatypeBiomemap(), "Secondary biomemap")
		};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBiomemap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
	}
}
