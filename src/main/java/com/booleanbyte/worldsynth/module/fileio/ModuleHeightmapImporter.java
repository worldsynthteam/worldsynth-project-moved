package com.booleanbyte.worldsynth.module.fileio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FileParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;

public class ModuleHeightmapImporter extends AbstractModule {
	
	private File heightmapImageFile;
	private boolean cache = false;
	
	private double xOffset = 0;
	private double zOffset = 0;
	
	private float[][] cachedHeightmap = null;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		if(heightmapImageFile == null) {
			return null;
		}
		else if(!heightmapImageFile.exists()) {
			return null;
		}
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		
		float[][] fileInputMap = cachedHeightmap;
		if(cache && cachedHeightmap == null) {
			fileInputMap = cachedHeightmap = readeImageFromFile(heightmapImageFile);
		}
		else if(!cache) {
			fileInputMap = readeImageFromFile(heightmapImageFile);
		}
		
		int inputMapWidth = fileInputMap.length;
		int inputMapHeight = fileInputMap[0].length;
		
		float[][] requestedMap = new float[mpw][mpl];
		for(int u = 0; u < mpw; u++) {
			int lx = (int) Math.floor((double) u * res + x - xOffset);
			if(lx < 0 || lx >= inputMapWidth) {
				continue;
			}
			for(int v = 0; v < mpl; v++) {
				int lz = (int) Math.floor((double) v * res + z - zOffset);
				if(lz < 0 || lz >= inputMapHeight) {
					continue;
				}
				requestedMap[u][v] = fileInputMap[lx][lz];
			}
		}
		
		requestData.heightMap = requestedMap;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.IMPORTER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Heightmap")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		DoubleParameterSlider parameterX = new DoubleParameterSlider("X offset", 0, 1000, xOffset);
		DoubleParameterSlider parameterZ = new DoubleParameterSlider("Z offset", 0, 1000, zOffset);
		FileParameterField parameterHeightmatFile = new FileParameterField("Heightmap file", heightmapImageFile, false, false, new ExtensionFilter("PNG", "*.png"));
		BooleanParameterCheckbox parameterCache = new BooleanParameterCheckbox("Cache heightmap", cache);
		
		parameterX.addToGrid(pane, 0);
		parameterZ.addToGrid(pane, 1);
		parameterHeightmatFile.addToGrid(pane, 2);
		parameterCache.addToGrid(pane, 3);
		
		cachedHeightmap = null;
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			heightmapImageFile = parameterHeightmatFile.getValue();
			cache = parameterCache.getValue();
			xOffset = parameterX.getValue();
			zOffset = parameterZ.getValue();
		};
		
		return applyHandler;
	}
	
	private float[][] readeImageFromFile(File f) {
		float[][] heightmap = null;
		
		try {
			BufferedImage image = ImageIO.read(f);
			double divident = 256.0;
			
			switch (image.getType()) {
			case BufferedImage.TYPE_USHORT_GRAY:
				divident = 65536.0;
				break;
			case BufferedImage.TYPE_USHORT_565_RGB:
				divident = 32.0;
				break;
			case BufferedImage.TYPE_USHORT_555_RGB:
				divident = 32.0;
				break;
			default:
				divident = 256.0;
				break;
			}
			
			heightmap = new float[image.getWidth()][image.getHeight()];
			for(int x = 0; x < heightmap.length; x++) {
				for(int y = 0; y < heightmap[0].length; y++) {
					double[] height = image.getRaster().getPixel(x, y, new double[4]);
					heightmap[x][y] = (float) (height[0] / divident);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return heightmap;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		if(heightmapImageFile != null) {
			paramenterElements.add(new Element("filepath", heightmapImageFile.getAbsolutePath()));
		}
		paramenterElements.add(new Element("cache", String.valueOf(cache)));
		paramenterElements.add(new Element("xoffset", String.valueOf(xOffset)));
		paramenterElements.add(new Element("zoffset", String.valueOf(zOffset)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("filepath")) {
				heightmapImageFile = new File(e.content);
			}
			else if(e.tag.equals("cache")) {
				cache = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("xoffset")) {
				xOffset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("zoffset")) {
				zOffset = Double.parseDouble(e.content);
			}
		}
	}
}
