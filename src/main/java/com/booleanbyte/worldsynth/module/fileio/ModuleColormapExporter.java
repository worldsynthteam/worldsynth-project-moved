package com.booleanbyte.worldsynth.module.fileio;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeColormap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.extent.WorldExtent;
import com.booleanbyte.worldsynth.extent.WorldExtentParameter;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FileParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;

public class ModuleColormapExporter extends AbstractModule {
	
	private WorldExtentParameter exportExtent;
	private File exportFile = new File(System.getProperty("user.home") + "/colormap_export.png");
	
	@Override
	protected void postInit() {
		exportExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("colormap");
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		
		inputRequests.put("colormap", new ModuleInputRequest(getInput(0), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.EXPORTER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Colormap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Troughput", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = exportExtent.getDropdownSelector("Export extent");
		FileParameterField parameterFile = new FileParameterField("Save file", exportFile, true, false, new ExtensionFilter("PNG", "*.png"));
		
		Button exportButton = new Button("Export colormap");
		exportButton.setOnAction(e -> {
			if(parameterExtent.getValue() == null) {
				return;
			}
			
			float[][][] colormap = getColormap(parameterExtent.getValue());
			writeImageToFile(colormap, parameterFile.getValue());
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterFile.addToGrid(pane, 1);
		pane.add(exportButton, 1, 2);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			exportExtent.setExtent(parameterExtent.getValue());
			exportFile = parameterFile.getValue();
		};
		
		return applyHandler;
	}
	
	private float[][][] getColormap(WorldExtent extent) {
		ModuleInputRequest request = new ModuleInputRequest(getInput(0), new DatatypeColormap(extent.getX(), extent.getZ(), extent.getWidth(), extent.getLength()));
		DatatypeColormap cmd = (DatatypeColormap) buildInputData(request);
		return cmd.colorMap;
	}
	
	private void writeImageToFile(float[][][] colormap, File f) {
		try {
			BufferedImage image = new BufferedImage(colormap.length, colormap[0].length, BufferedImage.TYPE_INT_RGB);
			for(int x = 0; x < colormap.length; x++) {
				System.out.println("Converting to buffer: " + (int)(100.0f*(float)x/(float)colormap.length) + "%");
				for(int y = 0; y < colormap[0].length; y++) {
					Color c = new Color(colormap[x][y][0], colormap[x][y][1], colormap[x][y][2]);
					image.setRGB(x, y, c.getRGB());
				}
			}
			ImageIO.write(image, "png", f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("extent", exportExtent.getExtentAsString()));
		paramenterElements.add(new Element("file", exportFile.getPath()));
		
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("extent")) {
				exportExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("file")) {
				exportFile = new File(e.content);
			}
		}
	}
}
