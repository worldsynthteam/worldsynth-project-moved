package com.booleanbyte.worldsynth.module.fileio;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeColormap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FileParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;

public class ModuleColormapImporter extends AbstractModule {
	
	private File colormapImageFile;
	private boolean cache = false;
	
	private double xOffset = 0;
	private double zOffset = 0;
	
	private float[][][] cachedColormap = null;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		if(colormapImageFile == null) {
			return null;
		}
		else if(!colormapImageFile.exists()) {
			return null;
		}
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		
		float[][][] fileInputMap = cachedColormap;
		if(cache && cachedColormap == null) {
			fileInputMap = cachedColormap = readeImageFromFile(colormapImageFile);
		}
		else if(!cache) {
			fileInputMap = readeImageFromFile(colormapImageFile);
		}
		
		int inputMapWidth = fileInputMap.length;
		int inputMapHeight = fileInputMap[0].length;
		
		float[][][] requestedMap = new float[mpw][mpl][3];
		for(int u = 0; u < mpw; u++) {
			int lx = (int) Math.floor((double) u * res + x - xOffset);
			if(lx < 0 || lx >= inputMapWidth) {
				continue;
			}
			for(int v = 0; v < mpl; v++) {
				int lz = (int) Math.floor((double) v * res + z - zOffset);
				if(lz < 0 || lz >= inputMapHeight) {
					continue;
				}
				requestedMap[u][v] = fileInputMap[lx][lz];
			}
		}
		
		requestData.colorMap = requestedMap;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.IMPORTER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Colormap")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		DoubleParameterSlider parameterX = new DoubleParameterSlider("X offset", 0, 1000, xOffset);
		DoubleParameterSlider parameterZ = new DoubleParameterSlider("Z offset", 0, 1000, zOffset);
		FileParameterField parameterHeightmatFile = new FileParameterField("Heightmap file", colormapImageFile, false, false, new ExtensionFilter("PNG", "*.png"));
		BooleanParameterCheckbox parameterCache = new BooleanParameterCheckbox("Cache colormap", cache);
		
		parameterX.addToGrid(pane, 0);
		parameterZ.addToGrid(pane, 1);
		parameterHeightmatFile.addToGrid(pane, 2);
		parameterCache.addToGrid(pane, 3);
		
		cachedColormap = null;
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			colormapImageFile = parameterHeightmatFile.getValue();
			cache = parameterCache.getValue();
			xOffset = parameterX.getValue();
			zOffset = parameterZ.getValue();
		};
		
		return applyHandler;
	}
	
	private float[][][] readeImageFromFile(File f) {
		float[][][] heightmap = null;
		
		try {
			BufferedImage image = ImageIO.read(f);
			
			heightmap = new float[image.getWidth()][image.getHeight()][3];
			for(int x = 0; x < heightmap.length; x++) {
				for(int y = 0; y < heightmap[0].length; y++) {
					Color c = new Color(image.getRGB(x, y));
					heightmap[x][y][0] = (float) c.getRed() / 255.0F;
					heightmap[x][y][1] = (float) c.getGreen() / 255.0F;
					heightmap[x][y][2] = (float) c.getBlue() / 255.0F;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return heightmap;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		if(colormapImageFile != null) {
			paramenterElements.add(new Element("filepath", colormapImageFile.getAbsolutePath()));
		}
		paramenterElements.add(new Element("cache", String.valueOf(cache)));
		paramenterElements.add(new Element("xoffset", String.valueOf(xOffset)));
		paramenterElements.add(new Element("zoffset", String.valueOf(zOffset)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("filepath")) {
				colormapImageFile = new File(e.content);
			}
			else if(e.tag.equals("cache")) {
				cache = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("xoffset")) {
				xOffset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("zoffset")) {
				zOffset = Double.parseDouble(e.content);
			}
		}
	}
}
