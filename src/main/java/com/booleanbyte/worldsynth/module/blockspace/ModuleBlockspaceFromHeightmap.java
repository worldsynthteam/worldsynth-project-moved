package com.booleanbyte.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeBlockspace;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.material.Material;
import com.booleanbyte.worldsynth.material.MaterialRegistry;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.MaterialParameterSelector;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleBlockspaceFromHeightmap extends AbstractModule {
	
	private float lowClamp = 0;
	private float highClamp = 1;
	
	private Material material = MaterialRegistry.getDefaultMaterial();
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double y = requestData.y;
		double height = requestData.height;
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		int[][][] blockspaceMaterialId = new int[spw][sph][spl];
		
		float[][] inputMap0 = null;
		float[][] inputMap1 = null;
		
		if(inputs.get("high") != null) {
			inputMap0 = ((DatatypeHeightmap) inputs.get("high")).heightMap;
		}
		if(inputs.get("low") != null) {
			inputMap1 = ((DatatypeHeightmap) inputs.get("low")).heightMap;
		}
		
		float fy = (float)y/255.0f;
		float fheight = (float)height/255.0f;
		
		int materialId = material.getInternalId();
		
		for(int u = 0; u < spw; u++) {
			for(int w = 0; w < spl; w++) {
				
				float instantMinHeight = Math.max(fy, lowClamp);
				float instantMaxHeight = Math.min(fy+fheight, highClamp);
				if(inputMap0 != null) {
					instantMaxHeight = Math.min(instantMaxHeight, inputMap0[u][w]);
				}
				if(inputMap1 != null) {
					instantMinHeight = Math.max(instantMinHeight, inputMap1[u][w]);
				}
				
				int minArrayHeight = (int)((instantMinHeight-fy)*255.0f/res);
				int maxArrayHeight = (int)((instantMaxHeight-fy)*255.0f/res);
				
				for(int v = minArrayHeight; v < maxArrayHeight; v++) {
					blockspaceMaterialId[u][v][w] = materialId;
				}
			}
		}
		
		requestData.blockspaceMaterialId = blockspaceMaterialId;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace vbrd = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vbrd.x, vbrd.z, vbrd.width, vbrd.length, vbrd.resolution);
		
		inputRequests.put("high", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("low", new ModuleInputRequest(getInput(1), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace from heightmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "High clamp"),
				new ModuleInput(new DatatypeHeightmap(), "Low clamp")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		FloatParameterSlider parameterHighClamp = new FloatParameterSlider("Low clamp", 0.0f, 1.0f, highClamp);
		FloatParameterSlider parameterLowClamp = new FloatParameterSlider("Low clamp", 0.0f, 1.0f, lowClamp);
		
		MaterialParameterSelector parameterMaterial = new MaterialParameterSelector("Material", material);
		
		try {
			parameterHighClamp.addToGrid(pane, 0);
			parameterLowClamp.addToGrid(pane, 1);
			parameterMaterial.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			highClamp = parameterHighClamp.getValue();
			lowClamp = parameterLowClamp.getValue();
			material = parameterMaterial.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("lowclamp", String.valueOf(lowClamp)));
		paramenterElements.add(new Element("highclamp", String.valueOf(highClamp)));
		paramenterElements.add(new Element("material", material.getIdName()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		int blockId = -1;
		byte blockMeta = 0;
		for(Element e: element.elements) {
			if(e.tag.equals("lowclamp")) {
				lowClamp = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("highclamp")) {
				highClamp = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("material")) {
				material = MaterialRegistry.getMaterial(e.content);
			}
			//FORREMOVAL A1.0.3 Remove reading of material from id and meta values
			else if(e.tag.equals("blockid")) {
				blockId = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("blockmeta")) {
				blockMeta = Byte.parseByte(e.content);
			}
		}
		
		if(blockId != -1) {
			material = MaterialRegistry.getMaterial(blockId, blockMeta);
		}
	}
}
