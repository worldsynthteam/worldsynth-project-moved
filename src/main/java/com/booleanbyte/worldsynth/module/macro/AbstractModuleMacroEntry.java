package com.booleanbyte.worldsynth.module.macro;

import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;

public abstract class AbstractModuleMacroEntry extends AbstractModuleMacroIO {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		if(getMacroModule() == null) {
			return null;
		}
		
		ModuleInputRequest inRequest = new ModuleInputRequest(getInput(0), request.data);
		return getMacroModule().buildMacroInput(inRequest);
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MACRO_ENTRY;
	}
}
