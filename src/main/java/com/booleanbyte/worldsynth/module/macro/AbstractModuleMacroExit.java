package com.booleanbyte.worldsynth.module.macro;

import java.util.HashMap;
import java.util.Map;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;

public abstract class AbstractModuleMacroExit extends AbstractModuleMacroIO {

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MACRO_EXIT;
	}
}
