package com.booleanbyte.worldsynth.module.macro;

import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleOutput;

public class ModuleHeightmapMacroExit extends AbstractModuleMacroExit {


	@Override
	public String getModuleName() {
		return "Macro exit (heightmap)";
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = {
				new ModuleInput(new DatatypeHeightmap(), "Macro exit [" + macroIoId + "]")
		};
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] out = {
				new ModuleOutput(new DatatypeHeightmap(), "Macro exit [" + macroIoId + "]", true)
		};
		return out;
	}
}
