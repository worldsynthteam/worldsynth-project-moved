package com.booleanbyte.worldsynth.module.macro;

import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleOutput;

public class ModuleHeightmapMacroEntry extends AbstractModuleMacroEntry {

	@Override
	public String getModuleName() {
		return "Macro entry (heightmap)";
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = {
				new ModuleInput(new DatatypeHeightmap(), "Macro entry [" + macroIoId + "]", true)
		};
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] out = {
				new ModuleOutput(new DatatypeHeightmap(), "Macro entry [" + macroIoId + "]")
		};
		return out;
	}

}
