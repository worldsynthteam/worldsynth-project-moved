package com.booleanbyte.worldsynth.module.featuremap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeFeaturemap;
import com.booleanbyte.worldsynth.datatype.DatatypeHeightmap;
import com.booleanbyte.worldsynth.event.module.ModuleApplyParametersEvent;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.IModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleCategory;
import com.booleanbyte.worldsynth.module.ModuleInput;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import com.booleanbyte.worldsynth.standalone.ui.parameters.LongParameterField;
import com.booleanbyte.worldsynth.synth.io.Element;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public class ModuleFeaturemapSimpleDistribution extends AbstractModule {
	private long seed;
	private double scale = 100.0;
	private double probability = 0.2;
	private double displacement = 0.0;
	
	private int permutationTables = 4;
	private int permutationSize = 1000;
	private int repeat = permutationSize;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[][] dp;
	
	public ModuleFeaturemapSimpleDistribution() {
		
		seed = new Random().nextLong();
		
		dp = createPermutatationTable(permutationSize, permutationTables, seed);
	}
	
	private int[][] createPermutatationTable(int size, int dim, long seed) {
		int[][] dp = new int[size*2][dim];
		
		for(int i = 0; i < dim; i++) {
			//Create a random generator with supplied seed
			Random r = new Random(seed + i);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for(int j = 0; j < size; j++) {
				valueTabel.add(j);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while(valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			for(int k = 0; k < dp.length; k++) {
				dp[k][i] = permutationTable[k%permutationSize];
			}
		}
		
		return dp;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double width = requestData.width;
		double length = requestData.length;
		double resolution = requestData.resolution;
		
		float[][] inputMap0_probabilityMap = null;
		
		if(inputs.get("probabilitymap") != null) {
			inputMap0_probabilityMap = ((DatatypeHeightmap) inputs.get("probabilitymap")).heightMap;
		}
		
		int minXPoint = (int)Math.floor(x/scale);
		int maxXPoint = (int)Math.ceil((x+width)/scale);
		int minZPoint = (int)Math.floor(z/scale);
		int maxZPoint = (int)Math.ceil((z+length)/scale);
		
		ArrayList<double[]> generatedPoints = new ArrayList<double[]>();
		ArrayList<Long> generatedPointSeeds = new ArrayList<Long>();
		
		for(int u = minXPoint; u < maxXPoint; u++) {
			for(int v = minZPoint; v < maxZPoint; v++) {
				int[] hashCoordinates = hashCoordianteAt(u, v);
				
				double xPos = u * scale;
				double zPos = v * scale;
				
				double xDisplace = (double)hash(hashCoordinates[0], hashCoordinates[1], 0) / permutationSize;
				double zDisplace = (double)hash(hashCoordinates[0], hashCoordinates[1], 1) / permutationSize;
				
				xPos += -scale*displacement/2.0f + xDisplace*scale*displacement;
				zPos += -scale*displacement/2.0f + zDisplace*scale*displacement;
				
				if(insideBoundary(xPos, zPos, requestData)) {
					double curentProbability = this.probability;
					if(inputMap0_probabilityMap != null) {
						curentProbability = inputMap0_probabilityMap[(int) ((xPos-x)/resolution)][(int) ((zPos-z)/resolution)];
					}
					if(hash(hashCoordinates[0], hashCoordinates[1], 2) < curentProbability * permutationSize) {
						generatedPoints.add(new double[]{xPos, zPos});
						generatedPointSeeds.add((long) hash(hashCoordinates[0], hashCoordinates[1], 3));
					}
				}
			}
		}
		
		double[][] points = new double[generatedPoints.size()][2];
		generatedPoints.toArray(points);
		
		long[] pointsSeeds = new long[generatedPointSeeds.size()];
		for(int i = 0; i < pointsSeeds.length; i++) {
			pointsSeeds[i] = generatedPointSeeds.get(i);
		}
		
		requestData.points = points;
		requestData.pointSeeds = pointsSeeds;
		return requestData;
	}
	
	private boolean insideBoundary(double x, double z, DatatypeFeaturemap requestData) {
		if(x < requestData.x || x >= requestData.x + requestData.width) {
			return false;
		}
		else if(z < requestData.z || z >= requestData.z + requestData.length) {
			return false;
		}
		return true;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(requestData.x, requestData.z, requestData.width, requestData.length, requestData.resolution);
		inputRequests.put("probabilitymap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}
	
	private int[] hashCoordianteAt(int x, int z) {
		
		if(repeat > 0) {
			x = x < 0 ? repeat+(x%repeat) : x%repeat;
			z = z < 0 ? repeat+(z%repeat) : z%repeat;
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int zi = (int)z & 255;
		
		return new int[] {xi, zi};
	}
	
	private int hash(int x, int y, int table) {
		return dp[dp[x][table]+y][table];
	}

	@Override
	public String getModuleName() {
		return "Simple distribution";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_FEATUREMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Probability")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturemap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterProbability = new DoubleParameterSlider("Probability", 0.0, 1.0, probability);
		DoubleParameterSlider parameterDisplacement = new DoubleParameterSlider("Displacement", 0.0, 1.0, displacement);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterProbability.addToGrid(pane, 1);
			parameterDisplacement.addToGrid(pane, 2);
			parameterSeed.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			probability = parameterProbability.getValue();
			displacement = parameterDisplacement.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			dp = createPermutatationTable(permutationSize, permutationTables, seed);
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("probability", String.valueOf(probability)));
		paramenterElements.add(new Element("displacement", String.valueOf(displacement)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				dp = createPermutatationTable(permutationSize, permutationTables, seed);
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("probability")) {
				probability = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("displacement")) {
				displacement = Double.parseDouble(e.content);
			}
		}
	}
}
