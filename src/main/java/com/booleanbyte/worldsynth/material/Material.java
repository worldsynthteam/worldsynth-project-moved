package com.booleanbyte.worldsynth.material;

import com.booleanbyte.worldsynth.common.color.WsColor;

import javafx.scene.paint.Color;

public class Material implements Comparable<Material> {
	private String name;
	private String idName;
	private int id;
	private byte meta;
	private WsColor color;
	
	public static final Material NULL = new Material("", "", -1, null);
	public static final Material AIR = new Material("Air", "minecraft:air", 0, new WsColor(0, 0, 0));
	
	public Material(String name, String idName, int id, WsColor color) {
		this(name, idName, id, (byte) -1, color);
	}
	
	public Material(String name, String idName, int id, int meta, WsColor color) {
		this.name = name;
		this.idName = idName;
		this.id = id;
		this.meta = (byte) meta;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIdName() {
		return idName;
	}
	
	public int getId() {
		return id;
	}
	
	public byte getMeta() {
		return meta;
	}
	
	public WsColor getWsColor() {
		return color;
	}
	
	public Color getFxColor() {
		return getWsColor().getFxColor();
	}
	
	public int getInternalId() {
		return MaterialRegistry.getInternalId(this);
	}
	
	@Override
	public String toString() {
		return getName();
	}

	@Override
	public int compareTo(Material comp) {
		return getName().compareTo(comp.getName());
	}
}
