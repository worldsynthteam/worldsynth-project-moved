package com.booleanbyte.worldsynth.common;

import java.io.File;

public class Commons {
	
	public static File getExecutionDirectory() {
		String path = Commons.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		if(path.endsWith("bin/")) {
			path = path.substring(0, path.lastIndexOf("/bin/"));
		}
		else {
			path = path.substring(0, path.lastIndexOf("/"));
		}
		return new File(path);
	}
	
	public static boolean systemIsWindows() {
		String OS = System.getProperty("os.name").toLowerCase();
		return OS.contains("windows");
	}

	public static boolean systemIsMac() {
		String OS = System.getProperty("os.name").toLowerCase();
		return OS.contains("mac");
	}

	public static boolean systemIsUnix() {
		String OS = System.getProperty("os.name").toLowerCase();
		return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
	}

	public static boolean systemIsSolaris() {
		String OS = System.getProperty("os.name").toLowerCase();
		return OS.contains("sunos");
	}
	
	public static float[][] fill(float[][] array2d, float d) {
		for (int i = 0, width = array2d.length; i < width; i++) {
			for (int j = 0, length = array2d[0].length; j < length; j++) {
				array2d[i][j] = d;
			}
		}
		return array2d;
	}
	
//	public static double[][] cast2d(float[][] array2f) {
//		double[][] array2d= new double[array2f.length][array2f[0].length];
//		for (int i = 0, width = array2f.length; i < width; i++) {
//			for (int j = 0, length = array2f[0].length; j < length; j++) {
//				array2d[i][j] = array2f[i][j];
//			}
//		}
//		return array2d;
//	}
}
