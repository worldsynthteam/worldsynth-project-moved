package com.booleanbyte.worldsynth.common.math;

public class MathHelperScalar {
	
	public static double clamp(double a, double min, double max) {
		return Math.min(Math.max(a, min), max);
	}
}
