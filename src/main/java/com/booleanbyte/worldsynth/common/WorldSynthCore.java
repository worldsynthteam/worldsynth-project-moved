package com.booleanbyte.worldsynth.common;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map.Entry;

import com.booleanbyte.worldsynth.biome.BiomeRegistry;
import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.datatype.DatatypeMultitype;
import com.booleanbyte.worldsynth.event.build.BuildStatus.BuildState;
import com.booleanbyte.worldsynth.material.MaterialRegistry;
import com.booleanbyte.worldsynth.event.build.BuildStatusEvent;
import com.booleanbyte.worldsynth.event.build.BuildStatusListener;
import com.booleanbyte.worldsynth.module.AbstractModule;
import com.booleanbyte.worldsynth.module.ModuleInputRequest;
import com.booleanbyte.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import com.booleanbyte.worldsynth.module.ModuleMacro;
import com.booleanbyte.worldsynth.module.ModuleOutput;
import com.booleanbyte.worldsynth.module.ModuleOutputRequest;
import com.booleanbyte.worldsynth.module.NativeModuleRegister;
import com.booleanbyte.worldsynth.modulewrapper.ModuleConnector;
import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapper;
import com.booleanbyte.worldsynth.modulewrapper.ModuleWrapperIO;
import com.booleanbyte.worldsynth.synth.Synth;
import com.booleanbyte.worldsynth.synth.io.Element;

public class WorldSynthCore {
	
	public static NativeModuleRegister moduleRegister;
	
	public WorldSynthCore(File worldSynthConfigDirectory) {
		//Initialize the biome registry
		new BiomeRegistry(worldSynthConfigDirectory);
		//Initialize  the material registry
		new MaterialRegistry(worldSynthConfigDirectory);
		
		//Register modules
		moduleRegister = new NativeModuleRegister(worldSynthConfigDirectory);
	}
	
	public static AbstractDatatype getModuleOutput(Synth synth, ModuleWrapper wrapper, ModuleOutputRequest request, BuildStatusListener buildListener) {
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.REGISTERED, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Get the input requests the module wants for building the output requested
		//and make sure all requests are unique instances.
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		for(Entry<String, ModuleInputRequest> entry: wrapper.module.getInputRequests(request).entrySet()) {
			if(entry == null) {
				continue;
			}
			inputRequests.put(entry.getKey(), new ModuleInputRequest(entry.getValue().getInput(), entry.getValue().getData().clone()));
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.TRANSLATING_INPUTREQUESTS_TO_OUTPUTREQUESTS, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Translate the input requests to output requests
		HashMap<String, ModuleOutputRequest> outputRequests = new HashMap<String, ModuleOutputRequest>();
		HashMap<String, ModuleWrapper> outputRequestsModuleWrapper = new HashMap<String, ModuleWrapper>();
		for(Entry<String, ModuleInputRequest> entry: inputRequests.entrySet()) {
			//Transform inputrequests into output requests if possible
			ModuleWrapperIO input = wrapper.getWrapperIoByModuleIo(entry.getValue().getInput());
			ModuleConnector[] connectors = synth.getConnectorsByWrapperIo(input);
			if(connectors.length == 0) {
				continue;
			}
			ModuleConnector connector = connectors[0];
			ModuleWrapper requestedWrapper = connector.outputWrapper;
			ModuleOutput requestedModuleOutput = (ModuleOutput) connector.outputWrapperIo.getIO();
			
			ModuleOutputRequest newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, entry.getValue().getData());
			if(entry.getValue().getData() instanceof DatatypeMultitype) {
				for(AbstractDatatype datatype: ((DatatypeMultitype) entry.getValue().getData()).getDatatypes()) {
					if(requestedModuleOutput.getData().getClass().equals(datatype.getClass())) {
						newOutputRequest = new ModuleOutputRequest(requestedModuleOutput, datatype);
						break;
					}
				}
			}
			outputRequests.put(entry.getKey(), newOutputRequest);
			outputRequestsModuleWrapper.put(entry.getKey(), requestedWrapper);
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.PREPARING_INPUT_DATA, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Invoke the outputrequests
		HashMap<String, AbstractDatatype> data = new HashMap<String, AbstractDatatype>();
		for(Entry<String, ModuleOutputRequest> entry: outputRequests.entrySet()) {
			AbstractDatatype newData = getModuleOutput(synth, outputRequestsModuleWrapper.get(entry.getKey()), entry.getValue(), buildListener);
			if(newData != null) {
				data.put(entry.getKey(), newData);
			}
		}
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Build the module wrapped
		AbstractDatatype output = wrapper.module.buildModule(data, request);
		
		if(buildListener != null) {
			buildListener.buildUpdate(new BuildStatusEvent(BuildState.DONE_BUILDING, "", wrapper, request, Thread.currentThread(), Thread.currentThread()));
		}
		
		//Return the data from the build
		return output;
	}
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleClass == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, Element moduleElement, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleClass == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromElement(moduleElement);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleEntry.getModuleClass() == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, Element moduleElement, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		if(moduleEntry.getModuleClass() == ModuleMacro.class && wrapper != null) {
			return new ModuleMacro(wrapper);
		}
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromElement(moduleElement);
		return moduleInstance;
	}
}
