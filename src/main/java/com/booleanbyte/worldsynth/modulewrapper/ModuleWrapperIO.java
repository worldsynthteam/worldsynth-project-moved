package com.booleanbyte.worldsynth.modulewrapper;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;
import com.booleanbyte.worldsynth.module.ModuleIO;

public class ModuleWrapperIO {
	
	private ModuleIO io;
	private boolean isInput;
	
	public static final float IO_RENDERSIZE = 10;
	
	/**
	 * Relative position in module wrapper
	 */
	public float posX;
	
	/**
	 * Relative position in module wrapper
	 */
	public float posY;
	
	public ModuleWrapperIO(ModuleIO io, float posX, float posY, boolean isInput) {
		this.io = io;
		this.posX = posX;
		this.posY = posY;
		this.isInput = isInput;
	}
	
	public boolean isInput() {
		return isInput;
	}

	public AbstractDatatype getDatatype() {
		return io.getData();
	}
	
	public ModuleIO getIO() {
		return io;
	}
	
	public String getName() {
		return io.getName();
	}
	
	@Override
	public String toString() {
		return io.getName() + " <" + io.getData().getDatatypeName() + ">";
	}
}
