package com.booleanbyte.worldsynth.standalone.ui.preview;

import com.booleanbyte.worldsynth.datatype.AbstractDatatype;

import javafx.scene.layout.Pane;

public abstract class AbstractPreviewRender extends Pane {

	public AbstractPreviewRender() {
		setPrefSize(450, 450);
	}
	
	public abstract void pushDataToRender(AbstractDatatype data);
}
