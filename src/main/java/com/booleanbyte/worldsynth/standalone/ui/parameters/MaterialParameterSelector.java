package com.booleanbyte.worldsynth.standalone.ui.parameters;

import com.booleanbyte.worldsynth.material.Material;
import com.booleanbyte.worldsynth.material.MaterialRegistry;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class MaterialParameterSelector {
	private Material value;
	
	private Label nameLable;
	private ComboBox<Material> parameterDropdownSelector = new ComboBox<Material>();
	
	public MaterialParameterSelector(String lable, Material initValue) {
		this.value = initValue;
		nameLable = new Label(lable);
		
		for(Material material: MaterialRegistry.getMaterialsAlphabetically()) {
			parameterDropdownSelector.getItems().add(material);
		}
		parameterDropdownSelector.getSelectionModel().select(value);
		
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<Material>() {

			@Override
			public void changed(ObservableValue<? extends Material> observable, Material oldValue, Material newValue) {
				value = newValue;
			}
		});
	}
	
	public Material getValue() {
		return value;
	}
	
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
