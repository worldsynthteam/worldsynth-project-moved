package com.booleanbyte.worldsynth.standalone.ui.parameters;

import com.booleanbyte.worldsynth.extent.ExtentEvent;
import com.booleanbyte.worldsynth.extent.WorldExtent;
import com.booleanbyte.worldsynth.extent.WorldExtentManager;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class ExtentParameterDropdownSelector {
	private WorldExtent value;

	private Label nameLable;
	private ComboBox<WorldExtent> parameterDropdownSelector;

	private boolean ignoreSelectionChange = false;

	public ExtentParameterDropdownSelector(String lable, WorldExtentManager extentManager, WorldExtent initValue) {
		// FIXME The event handler is never removed (potential memory leak?)
		extentManager.addEventHandler(ExtentEvent.ANY, e -> {
			if(e.getEventType() == ExtentEvent.EXTENT_REMOVED && e.getExtent() == value) {
				value = null;
			}
			ignoreSelectionChange = true;
			setExtentDropdownItems(extentManager.getObservableExtentsList());
			parameterDropdownSelector.setValue(value);
			ignoreSelectionChange = false;
		});

		value = initValue;

		nameLable = new Label(lable);
		parameterDropdownSelector = new ComboBox<WorldExtent>();
		setExtentDropdownItems(extentManager.getObservableExtentsList());

		parameterDropdownSelector.getSelectionModel().select(value);

		parameterDropdownSelector.setOnAction(e -> {
			if(!ignoreSelectionChange) {
				value = parameterDropdownSelector.getValue();
			}
		});
	}

	private void setExtentDropdownItems(ObservableList<WorldExtent> items) {
		parameterDropdownSelector.getItems().clear();
		for(WorldExtent par: items) {
			parameterDropdownSelector.getItems().add(par);
		}
	}

	public WorldExtent getValue() {
		return value;
	}

	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
