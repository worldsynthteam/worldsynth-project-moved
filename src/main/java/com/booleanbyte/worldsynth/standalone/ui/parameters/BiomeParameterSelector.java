package com.booleanbyte.worldsynth.standalone.ui.parameters;

import com.booleanbyte.worldsynth.biome.Biome;
import com.booleanbyte.worldsynth.biome.BiomeRegistry;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class BiomeParameterSelector {
	private Biome value;
	
	private Label nameLable;
	private ComboBox<Biome> parameterDropdownSelector = new ComboBox<Biome>();
	
	public BiomeParameterSelector(String lable, Biome initValue) {
		this.value = initValue;
		nameLable = new Label(lable);
		
		for(Biome biome: BiomeRegistry.getBiomesAlphabetically()) {
			parameterDropdownSelector.getItems().add(biome);
		}
		parameterDropdownSelector.getSelectionModel().select(value);
		
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<Biome>() {

			@Override
			public void changed(ObservableValue<? extends Biome> observable, Biome oldValue, Biome newValue) {
				value = newValue;
			}
		});
	}
	
	public Biome getValue() {
		return value;
	}
	
	public void addToGrid(GridPane pane, int row) throws Exception {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
