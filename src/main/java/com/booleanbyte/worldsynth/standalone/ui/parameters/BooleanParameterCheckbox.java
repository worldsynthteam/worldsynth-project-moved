package com.booleanbyte.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class BooleanParameterCheckbox {
	private boolean value;
	
	private Label nameLable;
	private CheckBox parameterCheckbox;
	
	public BooleanParameterCheckbox(String lable, boolean initValue) {
		this.value = initValue;
		
		nameLable = new Label(lable);
		parameterCheckbox = new CheckBox();
		parameterCheckbox.setSelected(initValue);
		
		parameterCheckbox.selectedProperty().addListener(new javafx.beans.value.ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				value = parameterCheckbox.isSelected();
			}
		});
	}
	
	public boolean getValue() {
		return value;
	}
	
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterCheckbox, 1, row);
	}
}
