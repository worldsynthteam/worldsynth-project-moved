package com.booleanbyte.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DoubleParameterField {
	private double value;
	
	private Label nameLable;
	private TextField parameterField;
	
	public DoubleParameterField(String lable, double initValue) {
		this.value = initValue;
				
		nameLable = new Label(lable);
		parameterField = new TextField(String.valueOf(value));
		parameterField.setPrefColumnCount(40);
		
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				applyChange();
			}
			
			private void applyChange() {
				String text = parameterField.getText().replace(",", ".");
				try {
					value = Double.parseDouble(text);
					parameterField.setStyle(null);
				} catch (NumberFormatException exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
			}
		});
	}
	
	public double getValue() {
		return value;
	}
	
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterField, 1, row);
	}
}
